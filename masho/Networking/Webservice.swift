
//  Webservice.swift
////  Created by Appzoc on 24/04/19.
////  Copyright © 2019 AppZoc. All rights reserved.
////https://aerradakzo.cloudimg.io/v7/https://www.pipli.in/storage/app/banners/vuqh687WxTdgRmTSJqITk1dxVDotBP35e19IHtiY.jpeg?w=750&h=500
//https://www.masho.com/api_v2/?action=guestlogin
import UIKit
//import Kingfisher

fileprivate var userName:String = "dev2"
fileprivate var password:String = "aNother123"
//let baseURL =  "https://app.abayakart.com/abayakart/v1" // live URL
//let baseURL =  "https://app.masho.com/api_v6" // Test URL
let baseURL = "https://app.abayakart.com/api_v6"
class Webservice{
    static let imageBaseURL = ""

    private var credentials: String {
        let credentialString = "\(userName):\(password)".data(using: .utf8)!
        let encodedString = credentialString.base64EncodedString()
        return encodedString
    }

    private var basicAuthHeader: String {
        return "Basic \(credentials)"
    }

    //Dev URL : "https://www.masho.com/api_v4"
    //Live URL : "https://app.masho.com/"

    // account : dev - 204

    let inputURL:String
    let parameters:[String:Any]
   // let activityIndicator:PIActivityIndicator = PIActivityIndicator()

    init(url:String, parameters:[String:Any]) {
        self.inputURL = url
        self.parameters = parameters
    }

    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }


    func executePOST(shouldReturn: ReturnDataType, completion:@escaping (Any?,Bool,String) -> ()){
        guard let url = URL(string: "\(baseURL)\(inputURL)") else { return }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request.httpMethod = "POST"
    //    activityIndicator.start()
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
        }

        let postString = self.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(basicAuthHeader, forHTTPHeaderField: "Authorization")

        URLSession.shared.dataTask(with: request) { (data, response, error) in
         //   self.activityIndicator.stop()
            print("Response CODE : \( (response as? HTTPURLResponse)?.statusCode)")
            if error != nil{
                if shouldReturn == .data{
                    completion(nil,false,error?.localizedDescription ?? "")
                }else{
                    completion([:],false,error?.localizedDescription ?? "")
                }
            }
            do{
                if let _data = data{

                    if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }

                    if shouldReturn == .data{
                        completion(_data,true,"")
                    }else{
                        if let jsonData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                            completion(jsonData ,true,"")
                        }else{
                            completion([:],false,error?.localizedDescription ?? "")
                        }
                    }
                }else{
                    if shouldReturn == .data{
                        completion(nil,false,error?.localizedDescription ?? "")
                    }else{
                        completion([:],false,error?.localizedDescription ?? "")
                    }
                }
            }catch{
                print("URL : \(url) \n",error)
                completion([:],false,error.localizedDescription)
            }
            }.resume()
    }

    func executeGET(shouldReturn:ReturnDataType, completion:@escaping (Any?,Bool,String) -> ()){
        let parameterString = self.getPostString(params: parameters)
        var appendingParams:String = ""
        if parameterString.trimmingCharacters(in: .whitespaces).count != 0{
            appendingParams = "?" + parameterString
        }
        guard let url = URL(string: "\(baseURL)\(inputURL)" + appendingParams) else { return }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request.httpMethod = "GET"
        print("Get URL : ",url)
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
//        } catch let error {
//            print(error.localizedDescription)
//        }

       // let parameterString = self.getPostString(params: parameters)
       // request.httpBody = postString.data(using: .utf8)

      //  activityIndicator.start()
        request.setValue("application/x-www-form-urlencoded charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(basicAuthHeader, forHTTPHeaderField: "Authorization")

        URLSession.shared.dataTask(with: request) { (data, response, error) in
        //    self.activityIndicator.stop()
            if error != nil{
                if shouldReturn == .data{
                    completion(nil,false,error?.localizedDescription ?? "")
                }else{
                    completion([:],false,error?.localizedDescription ?? "")
                }
            }
            do{
                if let _data = data{
                    if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }else if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? Array<Any>{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }

                    if shouldReturn == .data{
                        completion(_data,true,"")
                    }else{
                        if let jsonData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                            completion(jsonData ,true,"")
                        }else{
                            completion([:],false,error?.localizedDescription ?? "")
                        }
                    }
                }else{
                    if shouldReturn == .data{
                        completion(nil,false,error?.localizedDescription ?? "")
                    }else{
                        completion([:],false,error?.localizedDescription ?? "")
                    }
                }
            }catch{
                print("URL : \(url) \n",error)
                completion([:],false,error.localizedDescription)
            }
            }.resume()
    }


    func executeMultipartPOST(session:URLSession = URLSession.shared, dataParameters:[String:Data], mimeType:String, shouldReturn: ReturnDataType, completion:@escaping (Any?,Bool,String) -> ()){
        guard let url = URL(string: "\(baseURL)\(inputURL)") else { return }
    //    activityIndicator.start()
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request.httpMethod = "POST"
        request.setValue(basicAuthHeader, forHTTPHeaderField: "Authorization")

        let boundary = "----------youwe"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let body = NSMutableData()
        let boundaryPrefix = "--\(boundary)\r\n"

        for (key, value) in parameters {
            body.appendString("\(boundaryPrefix)")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }

        for (key,value) in dataParameters{
            body.appendString("\(boundaryPrefix)")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"file\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(value)
            body.appendString("\r\n")
        }
        //body.appendString("--".appending(boundary.appending("--\r\n")))
        body.appendString("--\(boundary)--\r\n")
        request.httpBody = body as Data
        URLSession.shared.dataTask(with: request) { (data, response, error) in
        //    self.activityIndicator.stop()
            if error != nil{
                if shouldReturn == .data{
                    completion(nil,false,error?.localizedDescription ?? "")
                }else{
                    completion([:],false,error?.localizedDescription ?? "")
                }
            }
            do{
                if let _data = data{

                    if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .allowFragments) as? [String : Any]{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }

                    if shouldReturn == .data{
                        completion(_data,true,"")
                    }else{
                        if let jsonData = try JSONSerialization.jsonObject(with: _data, options: .allowFragments) as? [String : Any]{
                            completion(jsonData ,true,"")
                        }else{
                            completion([:],false,error?.localizedDescription ?? "")
                        }
                    }
                }else{
                    if shouldReturn == .data{
                        completion(nil,false,error?.localizedDescription ?? "")
                    }else{
                        completion([:],false,error?.localizedDescription ?? "")
                    }
                }
            }catch{
                print("URL : \(url) \n",error)
                completion([:],false,error.localizedDescription)
            }
            }.resume()
    }



    //MARK: Without Loading Indicators

    func executeGETWithoutLoading(shouldReturn:ReturnDataType, completion:@escaping (Any?,Bool,String) -> ()){
        let parameterString = self.getPostString(params: parameters)
        var appendingParams:String = ""
        if parameterString.trimmingCharacters(in: .whitespaces).count != 0{
            appendingParams = "?" + parameterString
        }
        guard let url = URL(string: "\(baseURL)\(inputURL)" + appendingParams) else { return }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request.httpMethod = "GET"
        print("Get URL : ",url)

        request.setValue("application/x-www-form-urlencoded charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(basicAuthHeader, forHTTPHeaderField: "Authorization")

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil{
                if shouldReturn == .data{
                    completion(nil,false,error?.localizedDescription ?? "")
                }else{
                    completion([:],false,error?.localizedDescription ?? "")
                }
            }
            do{
                if let _data = data{
                    if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }else if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? Array<Any>{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }

                    if shouldReturn == .data{
                        completion(_data,true,"")
                    }else{
                        if let jsonData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                            completion(jsonData ,true,"")
                        }else{
                            completion([:],false,error?.localizedDescription ?? "")
                        }
                    }
                }else{
                    if shouldReturn == .data{
                        completion(nil,false,error?.localizedDescription ?? "")
                    }else{
                        completion([:],false,error?.localizedDescription ?? "")
                    }
                }
            }catch{
                print("URL : \(url) \n",error)
                completion([:],false,error.localizedDescription)
            }
            }.resume()
    }


    func executePOSTWithoutLoading(shouldReturn: ReturnDataType, completion:@escaping (Any?,Bool,String) -> ()){
        guard let url = URL(string: "\(baseURL)\(inputURL)") else { return }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 180)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
        }

        let postString = self.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(basicAuthHeader, forHTTPHeaderField: "Authorization")

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil{
                if shouldReturn == .data{
                    completion(nil,false,error?.localizedDescription ?? "")
                }else{
                    completion([:],false,error?.localizedDescription ?? "")
                }
            }
            do{
                if let _data = data{

                    if let jsonDisplayData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                        print("\n\nURL : \(url)\n JSON Response : ",jsonDisplayData,"\n\n")
                    }

                    if shouldReturn == .data{
                        completion(_data,true,"")
                    }else{
                        if let jsonData = try JSONSerialization.jsonObject(with: _data, options: .mutableContainers) as? [String : Any]{
                            completion(jsonData ,true,"")
                        }else{
                            completion([:],false,error?.localizedDescription ?? "")
                        }
                    }
                }else{
                    if shouldReturn == .data{
                        completion(nil,false,error?.localizedDescription ?? "")
                    }else{
                        completion([:],false,error?.localizedDescription ?? "")
                    }
                }
            }catch{
                print("URL : \(url) \n",error)
                completion([:],false,error.localizedDescription)
            }
            }.resume()
    }

    enum ReturnDataType{
        case data
        case json
    }

}

//MARK:- Image Auth

//func getImageAuthorization() -> AnyModifier {
//    let modifier = AnyModifier { request in
//        var requestMutable = request
//        let credentialData = "\(userName):\(password)".data(using: String.Encoding.utf8)!
//        let base64Credentials = credentialData.base64EncodedString(options: [])
//
//        requestMutable.setValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
//
//        return requestMutable
//    }
//    return modifier
//}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
