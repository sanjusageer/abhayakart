//
//  Video.swift
//  auto play test
//
//  Created by WC-64 on 21/09/21.
//

import UIKit
import AVKit

class VideoPlayerManager : NSObject {
    static let shared = VideoPlayerManager()
    private override init() { }

    var avPlayer: AVPlayer?
    
    func setPlayer(player:AVPlayer) {
        avPlayer = player
    }
  
    func configPlayer(control:PlayerControls) {
        switch control {
        case .play:
            self.play()
        case .pause:
            self.pause()
        case .stop:
            self.stop()
        
        }
    }
    
    func play(){
        self.avPlayer?.play()
    }
    
    func pause() {
        self.avPlayer?.pause()
    }
    
    func stop() {
        
    }
    
}
