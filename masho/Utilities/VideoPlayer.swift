//
//  VideoPlayer.swift
//  auto play test
//
//  Created by WC-64 on 21/09/21.
//

import UIKit


class VideoPlayer : NSObject {
    
    var video    :                   String?
    var player   :                   VideoPlayerView?
    var url      :                   URL?
    var view     :                   UIView?
    
    static let shared = VideoPlayer()
    
    private override init() {
    }
    
    
    func setPlayer(inView view:UIView,video:String){
//        view.removeAllSubView()
//        self.view = nil
//        self.player = nil
       // self.view = view
       // guard let v = self.view else {return}
        guard let url = URL(string: video) else {return}
        self.url = url
        if player == nil {
            self.player = VideoPlayerView()
            self.player?.frame = view.frame
        }
        print("viwwwwww layer",view.subviews.count)
        if let v = view.subviews.first as? VideoPlayerView {
            guard let url = self.url else {
                print("no video url found")
                return}
            v.play(for: url)
        } else {
            DispatchQueue.main.async {
                view.addSubview(self.player!)
            }
           
        }
        player?.playToEndTime = nil
        
    }
    
    func set(video: String) {
        guard let url = URL(string: video) else {return}
        self.url = url
        player?.playToEndTime = nil
    }
    
    func play() {
       // self.viwPlayer.seek(to: .zero)
        guard let url = self.url else {
            print("no video url found")
            return}
        player?.play(for: url)
        player?.isMuted = true
        player?.isHidden = false
       
        player?.player?.rate = 1
        player?.stateDidChanged = { state in
            switch state {
            case .none:
                print("state none")
            case .loading:
                print("state loading")
            case .playing:
                print("playing")
            case .paused(playProgress: let playProgress, bufferProgress: let bufferProgress):
                print("paused")
            case .error(let err ):
                print("error",err.localizedDescription)
            }
        }
        
        player?.playToEndTime = { [weak self] in
            print("playToEndTime")
            guard let self = self else {return}
            self.player?.observe(player: nil)
            self.player?.observe(player: nil)
        }
    }
    
    func pause() {
        player?.pause(reason: .userInteraction)
    }
    
    func playFromCurrentPositon() {
        player?.player?.play()
    }

    
    
    func configPlayer(control:PlayerControls) {
        switch control {
        case .play:
            self.play()
        case .pause:
            self.pause()
        case .stop:
            self.stop()
        
        }
    }
    
    func stop() {
       
    }
    
    func ended(){
        
    }
    
    deinit {
        print("player removed")
    }
    
}

protocol PlayerDelegate:AnyObject {
    func playerListener(state:PlayerState)
}

enum PlayerControls {
    case play,pause,stop
}

enum PlayerState {
    case playing,paused,ended,queue,unstarted,buffering,unknown
}

/*
 var avPlayer: AVPlayer!
 var avPlayerLayer: AVPlayerLayer!
 var paused: Bool = false
 */

extension UIView {
    func removeAllSubView() {
        self.subviews.forEach { $0.removeFromSuperview() }
    }
}
