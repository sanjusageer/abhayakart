//
//  AddAddressListingCells.swift
//  masho
//
//  Created by Castler on 28/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class ConfirmLocationCVC:UICollectionViewCell{
    
}
class EditMapLocationCVC:UICollectionViewCell{
    @IBOutlet weak var FirstLb:UILabel!
    @IBOutlet weak var SecondLb:UILabel!
}
class FieldsCVC:UICollectionViewCell{
    @IBOutlet weak var placeholderLb: UILabel!
    @IBOutlet weak var ContentTF: UITextField!
    @IBOutlet weak var ContentTV: UITextView!
    @IBOutlet weak var ContentTVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var Flag: UIImageView!
    @IBOutlet weak var anchorView: UIView!
}
protocol addressTypeDelegate{
    func addressType(actype:Int)
}
class AddressTypeCVC2:UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    var AddressData2 = [MHAddAddressModel]()
    var actype = 0
    var delegate : addressTypeDelegate!
    @IBOutlet weak var AddressTypeCV: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if AddressData2.count != 0{
            return AddressData2[0].arrayValue.count
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressTypeCVC", for: indexPath) as! AddressTypeCVC
        if AddressData2.count != 0{
            cell.addressTypeLb.text = AddressData2[0].arrayValue[indexPath.row]
            if actype == indexPath.row{
                cell.addressTypeLb.textColor = UIColor(hex: "\(AddressData2[0].btndata.color!)ff")
                cell.BGView.layer.borderColor = UIColor(hex: "\(AddressData2[0].btndata.background!)ff")?.cgColor
                cell.BGView.backgroundColor = UIColor(hex: "\(AddressData2[0].btndata.background!)ff")
            }else{
                cell.addressTypeLb.textColor = UIColor.lightGray
                cell.BGView.layer.borderColor = UIColor.lightGray.cgColor
                cell.BGView.backgroundColor = UIColor.white
            }
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actype = indexPath.row
        collectionView.reloadData()
        delegate.addressType(actype: actype)
    }
}
class AddressTypeCVC:UICollectionViewCell{
    @IBOutlet weak var BGView: BaseView!
    @IBOutlet weak var addressTypeLb: UILabel!
    
}
class SaveTypeCVC:UICollectionViewCell{
    @IBOutlet weak var CheckIcon: UIImageView!
    @IBOutlet weak var CheckLb: UILabel!
    @IBOutlet weak var CheckBoxBTN: UIButton!

}
class SaveAddressCVC:UICollectionViewCell{
    @IBOutlet weak var BGView: BaseView!
    @IBOutlet weak var TitleLb: UILabel!
}
