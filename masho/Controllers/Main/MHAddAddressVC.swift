//
//  MHAddAddressVC.swift
//  masho
//
//  Created by Appzoc on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
protocol displayNewVCdelegate {
    func displayNewVC(VC: UIViewController)
}
//class MHAddAddressVC: UIViewController,displayNewVCdelegate,selectedLocationDelegate,UITextFieldDelegate {
//    func displayNewVC(VC: UIViewController) {
//        delegate.displayNewVC(VC: VC)
//    }
//    @IBOutlet weak var optionsTV:UITableView!
//    @IBOutlet weak var MHAddAddress: UICollectionView!
//    @IBOutlet weak var PopUpView: UIView!
//    @IBOutlet weak var PopUpBottomView: UIView!
//    @IBOutlet weak var EmptyView: UIView!
//    @IBOutlet weak var SearchTF: UITextField!
//    var ForEdit = false
//    var forAddNewAddress = false
//    var actype = 0
//    var UserID : String = ""
//    var guest_id : Int = 0
//    var TableID : String = ""
//    var CountryCode : String = ""
//    var SelectedOption = 0
//    var setAddress = 0
//    var mapLocationData = [String:Any]()
//    var AddressDataForEdit : viewAddress_addressDetailsModel!
//    var AddressData :MHaddNewAddressModel!
//    var AddressData1 = [MHAddAddressModel]()
//    var AddressData2 = [MHAddAddressModel]()
//    var AddressData3 = [MHAddAddressModel]()
//    var delegate : displayNewVCdelegate! = nil
////    var firstime : Bool = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
////        MHAddAddress.delegate = self
////        MHAddAddress.dataSource = self
//        CountryCode = UserDefaults.standard.value(forKey: "CountryCode") as? String ?? ""
//        getPlaceholders()
//        SearchTF.delegate = self
//        PopUpBottomView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
//        PopUpView.isHidden = true
////        MHAddAddress.isUserInteractionEnabled = false
//        // Do any additional setup after loading the view.
//    }
//    var locationSelected = false
//    @IBAction func savecheckBTNtapped(_ sender: UIButton) {
//
//    }
//    @IBAction func closePopupBTNtapped(_ sender: Any) {
//        SearchTF.resignFirstResponder()
//        SearchTF.text = ""
//        PopUpView.isHidden = true
//
//    }
//    @objc func doneButtonClicked(_ sender: Any) {
////        self.MHAddAddress.isScrollEnabled = false
////        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.MHAddAddress.isScrollEnabled = true
////        }
////        self.MHAddAddress.isUserInteractionEnabled = false
////        self.MHAddAddress.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
////        self.MHAddAddress.isUserInteractionEnabled = true
//    }
//    @IBAction func CurrentLocationBTNtapped(_ sender: Any) {
//
//        for i in 0 ..< AddressData1.count{
//            let cell = MHAddAddress.cellForItem(at:IndexPath(row:i, section:1)) as! FieldsCVC
//            cell.ContentTF.text = ""
//        }
//        let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHMapVC") as! MHMapVC
//        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: false)
//
//    }
//    @IBAction func setdefaultAddresssBTNtapped(_ sender: UIButton) {
//        let cell = MHAddAddress.cellForItem(at:IndexPath(row:0, section:3)) as! SaveTypeCVC
//        if setAddress == 0{
//            setAddress = 1
//            cell.CheckIcon.image = UIImage(named: "square")
//        }else{
//            cell.CheckIcon.image = UIImage(named: "Checked")
//            setAddress = 0
//        }
////        MHAddAddress.reloadSections(NSIndexSet(index: 3) as IndexSet)
//    }
//
//    @IBAction func SaveBTNtapped(_ sender: Any) {
//
//        print(ResultArray)
//        if ResultArray.isEmpty{
//            Banner.main.showBanner(title: "", subtitle: "Please fill required fileds", style: .danger)
//        }else{
//            if ResultArray["code"] as? String != "" && ResultArray["mobile"] as? String != "" && ResultArray["fullname"] as? String != "" && ResultArray["email"] as? String != "" && ResultArray["pincode"] as? String != "" && ResultArray["address"] as? String != "" && ResultArray["landmark"] as? String != "" && ResultArray["city"] as? String != "" && ResultArray["state"] as? String != "" && ResultArray["country"] as? String != "" && ResultArray["code"] != nil && ResultArray["mobile"] != nil && ResultArray["fullname"] != nil && ResultArray["email"] != nil && ResultArray["pincode"] != nil && ResultArray["address"] != nil && ResultArray["landmark"] != nil && ResultArray["city"] != nil && ResultArray["state"] != nil && ResultArray["country"] != nil{
//
//                for i in 0 ..< AddressData1.count{
//                    if AddressData1[i].fieldname == "email"{
//                        let cell = MHAddAddress.cellForItem(at:IndexPath(row:i, section:1)) as! FieldsCVC
//                        if BaseValidator.isValid(email: cell.ContentTF.text){
//                            saveAddress()
//                        }else{
//                            Banner.main.showBanner(title: "", subtitle: "Please enter a valid email", style: .danger)
//                        }
//                    }
//                }
//
//                }else{
//                    Banner.main.showBanner(title: "", subtitle: "Please fill required fileds", style: .danger)
//                }
//        }
//    }
//
//
//
//
//    var optionArray = [MHAddAddressOptionsModel]()
//    var optionArray1 = [MHAddAddressOptionsModel]()
//    var selectedOptionIndex = 0
//    var selectedOption = ""
//    @IBAction func OptionSelectedBTNtapped(_ sender: UIButton) {
//        SearchTF.resignFirstResponder()
//        let cell = MHAddAddress.cellForItem(at: IndexPath(item: selectedOptionIndex, section: 1)) as! FieldsCVC
//        if selectedOption != ""{
//            let words = selectedOption.split(separator: "(")
//            cell.ContentTF.text = String(words[0])
//            SearchTF.text = ""
//            PopUpView.isHidden = true
//        }else{
//            Banner.main.showBanner(title: "", subtitle: "Please select any option", style: .danger)
//        }
//
//    }
//    var ResultArray = [String:Any]()
//    @IBAction func endEditing(_ sender: UITextField) {
//        print(sender.tag)
//            let cell = MHAddAddress.cellForItem(at: IndexPath(item: sender.tag, section: 1)) as! FieldsCVC
//            ResultArray[AddressData1[sender.tag].fieldname] = cell.ContentTF.text!
//            print(ResultArray)
//
//    }
//    @IBAction func searchEditingChanged(_ sender: UITextField) {
//        if sender == SearchTF{
//            var searchedArray1 = [MHAddAddressOptionsModel]()
//            for item in optionArray1{
//                if item.countryname == nil {
//                    searchedArray1 = optionArray1.filter {$0.name.localizedCaseInsensitiveContains(SearchTF.text!) }
//                }else{
//                    searchedArray1 = optionArray1.filter { $0.countryname.localizedCaseInsensitiveContains(SearchTF.text!) || $0.name.localizedCaseInsensitiveContains(SearchTF.text!) }
//                }
//            }
//            optionArray.removeAll()
//            optionArray = (SearchTF.text?.isEmpty)! ? optionArray1 : searchedArray1
//            self.optionsTV.reloadData()
//        }
//    }
//    @IBAction func editingChanged(_ sender: UITextField) {
//        if AddressData1[sender.tag].fieldname == "mobile"{
//            let Limit = 10
//            let currentLength:Int = (sender.text?.count)!
//            print(currentLength)
//            if currentLength >= Limit{
//                sender.text = String(sender.text!.prefix(Limit))
////                sender.resignFirstResponder()
//            }
//        }
//    }
//    @IBAction func shouldBegin(_ sender: UITextField) {
//        self.MHAddAddress.isScrollEnabled = false
//    }
//
//    func locationSelect(LocationData: [String : Any]) {
//        locationSelected = true
//        mapLocationData = LocationData
////        firstime = true
//        MHAddAddress.reloadData()
//    }
//    {
//}
//extension MHAddAddressVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,addressTypeDelegate
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 5
//    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
////        if firstime{
//        if section == 0{
//            return 1
//        }else if section == 1{
//
//            if AddressData1.count != 0{
//                return AddressData1.count
//            }
//                return 1
//
//
//        }else if section == 2{
////            if AddressData2.count != 0{
////                return AddressData2[0].arrayValue.count
////            }else{
//                return 1
////            }
//        }else if section == 3{
//            if AddressData3.count != 0{
//                return AddressData3.count
//            }
//                return 1
//
//        }else if section == 4{
//            return 1
////        }
//        }else{
//            return 1
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print("indexPath : \(indexPath)")
//        if indexPath.section == 0{
//            if locationSelected == false{
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConfirmLocationCVC", for: indexPath) as! ConfirmLocationCVC
//                return cell
//            }else{
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditMapLocationCVC", for: indexPath) as! EditMapLocationCVC
//                cell.FirstLb.text = mapLocationData["city"] as? String
//                cell.SecondLb.text = mapLocationData["landmark"] as? String
//                return cell
//            }
//
//        }else if indexPath.section == 1{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FieldsCVC", for: indexPath) as! FieldsCVC
//            if AddressData1.count != 0{
//                if AddressData1[indexPath.row].fieldname == "country"{
//                    cell.Flag.isHidden = false
//                }else{
//                    cell.Flag.isHidden = true
//                }
//                if indexPath.row == 0{
//                    cell.ContentTF.becomeFirstResponder()
//                }
//                cell.ContentTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
//    //            if firstime{
//    //                if indexPath.row == AddressData1.count - 1{
//    //                    firstime = false
//    //                }
//
//                    cell.placeholderLb.text = AddressData1[indexPath.row].placeholder
//                if AddressData1[indexPath.row].fieldtype == "select"{
//                    cell.ContentTF.isUserInteractionEnabled = false
//                }else{
//                    cell.ContentTF.isUserInteractionEnabled = true
//                }
//                cell.ContentTF.tag = indexPath.row
//                if AddressData1[indexPath.row].fieldname == "mobile" || AddressData1[indexPath.row].fieldname == "pincode" {
//                    cell.ContentTF.keyboardType = .numberPad
//                }else{
//                    cell.ContentTF.keyboardType = .default
//                }
//                if self.ForEdit{
//                    if AddressData1[indexPath.row].fieldname == "code"{
//                        for item in AddressData1[indexPath.row].options{
//                            if item.value == AddressData1[indexPath.row].value{
//                                cell.ContentTF.text = item.name
//                            }
//                        }
//
//                    }else if AddressData1[indexPath.row].fieldname == "country"{
//                        for item in AddressData1[indexPath.row].options{
//                            if item.value == AddressData1[indexPath.row].value{
//                                cell.ContentTF.text = item.countryname
//                                cell.Flag.kf.setImage(with: URL(string: item.image))
//                            }
//                        }
//                    }else if AddressData1[indexPath.row].fieldname == "state"{
//                        for item in AddressData1[indexPath.row].options{
//                            if item.value == AddressData1[indexPath.row].value{
//                                cell.ContentTF.text = item.name
//                                break
//                            }
//                        }
//                    }else{
//                        cell.ContentTF.text = AddressData1[indexPath.row].value
//                    }
//
//                }
//                if forAddNewAddress{
//                    if AddressData1[indexPath.row].fieldname == "code"{
//                        for item in AddressData1[indexPath.row].options{
//                            if AddressData1[indexPath.row].value == item.value{
//                                cell.ContentTF.text = item.name
//                                self.ResultArray["code"] = item.value
//                            }
//                        }
//
//                    }
//                    if AddressData1[indexPath.row].fieldname == "country"{
//                        for item in AddressData1[indexPath.row].options{
//                            if AddressData1[indexPath.row].value == item.value{
//                                cell.ContentTF.text = item.countryname
//                                cell.Flag.kf.setImage(with: URL(string: item.image))
//                                self.ResultArray["country"] = item.value
//                            }
//                        }
//                    }
//                }
//                if locationSelected{
//                    self.ResultArray["state"] = self.mapLocationData["state"] as? String
//                    self.ResultArray["city"] = self.mapLocationData["city"] as? String
//                    self.ResultArray["landmark"] = self.mapLocationData["landmark"] as? String
//                    self.ResultArray["pincode"] = self.mapLocationData["pincode"] as? String
//
//                    if AddressData1[indexPath.row].fieldname == "code"{
//                        for item in AddressData1[indexPath.row].options{
//                            if item.countryname == self.mapLocationData["country"] as? String{
//                                cell.ContentTF.text = item.name
//                                self.ResultArray["country"] = item.value
//                            }
//                        }
//
//                    }
//                    if AddressData1[indexPath.row].fieldname == "country"{
//                        for item in AddressData1[indexPath.row].options{
//                            if item.countryname == self.mapLocationData["country"] as? String{
//                                cell.ContentTF.text = item.countryname
//                                cell.Flag.kf.setImage(with: URL(string: item.image))
//                                self.ResultArray["code"] = item.value
//                            }
//                        }
//                        cell.ContentTF.text = self.mapLocationData["country"] as? String
//                    }
//                    if AddressData1[indexPath.row].fieldname == "state"{
//                        cell.ContentTF.text = self.mapLocationData["state"] as? String
//                        for item in AddressData1[indexPath.row].options{
//                            if item.name == self.mapLocationData["state"] as? String{
//                                self.ResultArray["code"] = item.value ?? "75"
//                            }
//                        }
//                    }
//                    if AddressData1[indexPath.row].fieldname == "city"{
//                        cell.ContentTF.text = self.mapLocationData["city"] as? String
//                    }
//                    if AddressData1[indexPath.row].fieldname == "landmark"{
//                        cell.ContentTF.text = self.mapLocationData["landmark"] as? String
//                    }
//                    if AddressData1[indexPath.row].fieldname == "pincode"{
//                        cell.ContentTF.text = self.mapLocationData["pincode"] as? String
//                    }
//                }
//    //            }
//            }
//
//
//            return cell
//        }else if indexPath.section == 2{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressTypeCVC2", for: indexPath) as! AddressTypeCVC2
//            if self.AddressData2.count != 0{
//                cell.AddressData2 = self.AddressData2
//                cell.delegate = self
//                cell.actype = self.AddressData2[0].arrayValue.firstIndex(where: {$0 == self.AddressData2[0].value}) ?? 0
//                cell.AddressTypeCV.reloadData()
//            }
//            return cell
//        }else if indexPath.section == 3{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SaveTypeCVC", for: indexPath) as! SaveTypeCVC
//            if AddressData3.count != 0{
//                cell.CheckLb.text = AddressData3[indexPath.row].label
//                cell.CheckBoxBTN.tag = indexPath.row
//                if setAddress == 0{
//                    cell.CheckIcon.image = UIImage(named: "square")
//                }else{
//                    cell.CheckIcon.image = UIImage(named: "Checked")
//                }
//            }
//            return cell
//        }else{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SaveAddressCVC", for: indexPath) as! SaveAddressCVC
//            if AddressData != nil{
//                cell.BGView.backgroundColor = UIColor(hex: "\(AddressData.btndata.btndata.background!)ff")
//                cell.TitleLb.text = AddressData.btndata.label
//                cell.TitleLb.textColor = UIColor(hex: "\(AddressData.btndata.btndata.color!)ff")
//            }
//            return cell
//        }
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if indexPath.section == 0{
////            if AddressData != nil{
//                if AddressData.geolocation == 1{
//                    if locationSelected == false{
//                        return CGSize(width: collectionView.frame.width, height: 70)
//                    }else{
//                        return CGSize(width: collectionView.frame.width, height: 100)
//                    }
////                }else{
////                    return CGSize(width: 0, height: 0)
////                }
////            }else{
////                 return CGSize(width: 0, height: 0)
////            }
//
//        }else if indexPath.section == 1{
//            if AddressData1.count != 0{
//                if AddressData1[indexPath.row].size == "Large" && AddressData1[indexPath.row].fieldname != "code" && AddressData1[indexPath.row].fieldname != "mobile"{
//                    return CGSize(width: collectionView.frame.width, height: 75)
//                }else if AddressData1[indexPath.row].size == "Medium" && AddressData1[indexPath.row].fieldname != "code" && AddressData1[indexPath.row].fieldname != "mobile"{
//                    return CGSize(width: collectionView.frame.width / 2, height: 75)
//                }
//
//                if AddressData1[indexPath.row].fieldname == "code"{
//                    return CGSize(width: collectionView.frame.width / 3.5, height: 75)
//                }else if AddressData1[indexPath.row].fieldname == "mobile"{
//                    let width = collectionView.frame.width - (collectionView.frame.width / 3.5)
//                    return CGSize(width: width, height: 75)
//                }
//            }
//            return CGSize(width: collectionView.frame.width, height: 75)
//        }else if indexPath.section == 2{
//            return CGSize(width: collectionView.frame.width, height: 50)
//        }else if indexPath.section == 3{
//            return CGSize(width: collectionView.frame.width / 2, height: 70)
//        }else{
//            return CGSize(width: collectionView.frame.width, height: 60)
//        }
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.section == 1{
//            if AddressData1[indexPath.row].fieldtype == "select"{
//                let cell = collectionView.cellForItem(at: indexPath) as? FieldsCVC
//                cell?.ContentTF.resignFirstResponder()
//                optionArray.removeAll()
//
//                selectedOptionIndex = indexPath.row
//                optionArray = AddressData1[indexPath.row].options
//                optionArray1 = AddressData1[indexPath.row].options
//                optionsTV.reloadData()
//                PopUpView.isHidden = false
//                if cell?.ContentTF.text == ""{
//                    self.SelectedOption = 0
//                }else {
//                        if AddressData1[indexPath.row].fieldname == "code"{
//                            SelectedOption = AddressData1[indexPath.row].options.firstIndex {$0.name == cell?.ContentTF.text!}!
//                            optionsTV.scrollToRow(at: IndexPath(row: SelectedOption, section: 0), at: .middle, animated: true)
//                        }
//                        if AddressData1[indexPath.row].fieldname == "country"{
//                            SelectedOption = AddressData1[indexPath.row].options.firstIndex {$0.countryname == cell?.ContentTF.text!}!
//                            optionsTV.scrollToRow(at: IndexPath(row: SelectedOption, section: 0), at: .middle, animated: true)
//                        }
//                        if AddressData1[indexPath.row].fieldname == "state"{
//                            SelectedOption = AddressData1[indexPath.row].options.firstIndex {$0.name == cell?.ContentTF.text!} ?? 0
//                            optionsTV.scrollToRow(at: IndexPath(row: SelectedOption, section: 0), at: .middle, animated: true)
//                        }
//                }
//
//            }else{
////                cell.ContentTF.isUserInteractionEnabled = true
//            }
//
//        }
////        if indexPath.section == 2{
////            actype = indexPath.row
////            collectionView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet)
////        }
////        if indexPath.section == 3{
////            setAddress = indexPath.row
////            collectionView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet)
////        }
//
//    }
//    func addressType(actype: Int){
//        self.actype = actype
//    }
//}
//extension MHAddAddressVC:UITableViewDelegate,UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return optionArray.count
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "popupTVC") as! popupTVC
//        if AddressData1[selectedOptionIndex].fieldname == "country" {
//            cell.flag.isHidden = false
//            cell.flag.kf.setImage(with: URL(string: optionArray[indexPath.row].image))
//            cell.titleLb.text = optionArray[indexPath.row].countryname
//        }else if AddressData1[selectedOptionIndex].fieldname == "state" {
//            cell.flag.isHidden = true
//            cell.titleLb.text = optionArray[indexPath.row].name
//        }else{
//            cell.flag.isHidden = false
//            cell.flag.kf.setImage(with: URL(string: optionArray[indexPath.row].image))
//            cell.titleLb.text = "\(optionArray[indexPath.row].countryname!) \(optionArray[indexPath.row].name!)"
//        }
//        if SelectedOption == indexPath.row{
//            cell.Icon.isHidden = false
//            if AddressData1[selectedOptionIndex].fieldname == "country" {
//                selectedOption = optionArray[indexPath.row].countryname
//                ResultArray[AddressData1[selectedOptionIndex].fieldname] = optionArray[indexPath.row].value
//            }else{
//                selectedOption = optionArray[indexPath.row].name
//                ResultArray[AddressData1[selectedOptionIndex].fieldname] = optionArray[indexPath.row].value
//            }
//        }else{
//            cell.Icon.isHidden = true
//        }
//        return cell
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        SelectedOption = indexPath.row
//        tableView.reloadData()
//    }
//}
class popupTVC:UITableViewCell{
    @IBOutlet weak var flag:UIImageView!
    @IBOutlet weak var titleLb:UILabel!
    @IBOutlet weak var Icon:UIImageView!
}
//extension MHAddAddressVC{
//    func getPlaceholders(){
//        EmptyView.isHidden  = false
//        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
//        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
//        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
//        CountryCode = UserDefaults.standard.value(forKey: "CountryCode") as? String ?? ""
////https://www.masho.com/api?action=addnewaddress&userId=678&countryid=12&addressid=2968
//        var parameters = ["action":"addnewaddress",
//                          "userId":"\(UserID)",
//            "countryid": CountryCode,
//                          "tableid": TableID] as [String : Any]
//        if self.ForEdit{
//            parameters["addressid"] = self.AddressDataForEdit.address_id
//        }
////        self.view.alpha = 0.5
//        self.view.isUserInteractionEnabled = false
//        KVSpinnerView.show()
//        NetworkManager.webcallWithErrorCode(urlString: "https://www.masho.com/api/", methodeType: .post,parameter: parameters) { (status, response) in
//            print(response)
//            switch status {
//            case .noNetwork:
//                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
//            case .success :
//                print("success")
//                print(response["data"]!)
//                if let data = response as? [String: Any]{
//                    self.AddressData = MHaddNewAddressModel(fromData: data)
//                }
//                self.AddressData1 = self.AddressData.data.filter({$0.fieldtype == "text" || $0.fieldtype == "textarea" || $0.fieldtype == "select" || $0.fieldtype == "number"})
//                self.AddressData2 = self.AddressData.data.filter({$0.fieldtype == "radio"})
//                self.AddressData3 = self.AddressData.data.filter({$0.fieldtype == "checkbox"})
//                
//                if self.ForEdit{
//                    let index = self.AddressData2[0].arrayValue.firstIndex {$0 == self.AddressData2[0].value }
//                    if index == nil{
//                        self.actype = 0
//                    }else{
//                        self.actype = index!
//                    }
//                    for item in self.AddressData3{
//                        if item.value == "0"{
//                            self.setAddress = 0
//                        }else{
//                            self.setAddress = 1
//                        }
//                    }
//                    for item in self.AddressData1{
//                        self.ResultArray[item.fieldname] = item.value
//                    }
//                    self.ResultArray["addresstype"] = self.AddressData2[0].arrayValue[self.actype]
//                    self.ResultArray["setaddress"] = self.setAddress
//                }
////                self.MHAddAddress.reloadData()
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                DispatchQueue.main.async {
////                self.firstime = true
//                  self.MHAddAddress.reloadData()
//                }
////                let cell = self.MHAddAddress.cellForItem(at:IndexPath(row:0, section:1)) as! FieldsCVC
////                cell.ContentTF.becomeFirstResponder()
//                self.EmptyView.isHidden  = true
//            case .failure :
//                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
//            case .unknown:
//                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
//            }
//        }
//    }
//    func saveAddress(){
//        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
//        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
//        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
//        
//            //https://www.masho.com/api?action=saveaddress&userId=1437&tableid=UI7nDFH64KdR23m&fullname=Harikrishna&code=12&mobile=9847737810&email=add@qwe.op&city=Rajakkad&country=12&state=75&pincode=685619&address=Kunnelputhenpurackal Arivilamchal PO&landmark=Mangathotty &addresstype=Office&setaddress=1
//            var parameters = [
//                "action":"saveaddress",
//                "userId":"\(UserID)",
//                "code":ResultArray["code"]!,
//                "mobile":ResultArray["mobile"]!,
//                "fullname":ResultArray["fullname"]!,
//                "email":ResultArray["email"]!,
//                "pincode":ResultArray["pincode"]!,
//                "address":ResultArray["address"]!,
//                "landmark":ResultArray["landmark"]!,
//                "city":ResultArray["city"]!,
//                "state":ResultArray["state"]!,
//                "country":ResultArray["country"]!,
//                "addresstype":AddressData2[0].arrayValue[actype],
//                "setaddress": "\(setAddress)",
//                "tableid": TableID
//            ] as [String : Any]
//            if self.ForEdit{
//                parameters["addressid"] = self.AddressDataForEdit.address_id
//            }
//            self.view.alpha = 0.5
//            self.view.isUserInteractionEnabled = false
//            KVSpinnerView.show()
//            NetworkManager.webcallWithErrorCode(urlString: "https://www.masho.com/api/", methodeType: .post,parameter: parameters) { (status, response) in
//                print(response)
//                switch status {
//                case .noNetwork:
//                    print("network error")
//                    self.view.alpha = 1
//                    self.view.isUserInteractionEnabled = true
//                    KVSpinnerView.dismiss()
//                    Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
//                case .success :
//                    print("success")
//                    print(response["data"]!)
//                    self.view.alpha = 1
//                    self.view.isUserInteractionEnabled = true
//                    KVSpinnerView.dismiss()
//                    let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SelectaddressViewController") as! SelectaddressViewController
//                    vc.delegate = self
//                    self.delegate.displayNewVC(VC: vc)
//                case .failure :
//                    print("failure")
//                    self.view.alpha = 1
//                    self.view.isUserInteractionEnabled = true
//                    KVSpinnerView.dismiss()
//                    Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
//                case .unknown:
//                    print("unknown")
//                    self.view.alpha = 1
//                    self.view.isUserInteractionEnabled = true
//                    KVSpinnerView.dismiss()
//                    Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
//                }
//            }
//        
//    }
//}
