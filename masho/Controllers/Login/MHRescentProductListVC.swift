//
//  MHRescentProductListVC.swift
//  masho
//
//  Created by Appzoc on 19/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Firebase
import DropDown
var isUpdateRecent = false
class MHRescentProductListVC: UIViewController {
//    lazy var refreshControl = UIRefreshControl()
    var ProductArray = [MHrecentlyViewedbodyModel]()
    var categoryArray = [categoryListmodel]()
    let UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var currentProductId : String!
    var dropDown = DropDown()
   
    var isfromsimilar = true
    @IBOutlet weak var listEmptyIndicatorLBL: UILabel!
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    @IBOutlet weak var recentCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.initializeView()
        recentCV.refreshControl = UIRefreshControl()
        // add target to UIRefreshControl
        recentCV.refreshControl?.addTarget(self, action: #selector(callPullToRefresh(sender:)), for: .valueChanged)
        rescentList()
        logEvent()
        setDropdown()
        // Do any additional setup after loading the view.
    }
    @objc func callPullToRefresh(sender: UIRefreshControl){
            self.rescentList()
        }
    
    
    
    
    func logEvent(){
        Analytics.logEvent("Rescent_Product_List_Opened", parameters: [:])
    }
    override func viewWillAppear(_ animated: Bool) {
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == 0{
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
    }
    @IBAction func CartBTNTaooed(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//        WebViewVC.categoryArray = self.categoryListArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
   
    
        
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        if frompush {
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
        else {
          self.navigationController?.popViewController(animated: true)
        }
        
        
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
    }
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                    CartVC.categoryArray = self.categoryArray
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
//    private func initializeView(){
//           refreshControl.tintColor = .systemGray
//           refreshControl.addTarget(self,
//                                    action: #selector(refreshData(sender:)),
//                                    for: .valueChanged)
//           //recentCV.refreshControl = refreshControl
//        recentCV.refreshControl = refreshControl
//
//   }
//
//       @objc func refreshData(sender: UIRefreshControl) {
//           self.rescentList()
//           self.logEvent()
//           self.setDropdown()
//       }
}
extension MHRescentProductListVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,rescentProductsFavDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ProductArray.count == 0{
            DispatchQueue.main.async {
                self.listEmptyIndicatorLBL.isHidden = false
            }
        }else{
            DispatchQueue.main.async {
                self.listEmptyIndicatorLBL.isHidden = true
            }
        }
        return ProductArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RescentProductsCVC", for: indexPath) as! RescentProductsCVC
        cell.delegate = self
        cell.index = indexPath.row
        cell.ProductImage.kf.setImage(with: URL(string: "\(ProductArray[indexPath.row].imageUrl!)"))
        cell.ProductName.text = ProductArray[indexPath.row].productName
        cell.StrikePrice.attributedText = "\(ProductArray[indexPath.row].price!)".updateStrikeThroughFont(UIColor(hex: "\(ProductArray[indexPath.row].pricecolor!)ff")!)
        cell.StrikePrice.textColor = UIColor(hex: "\(ProductArray[indexPath.row].pricecolor!)ff")
        cell.PriceLb.text = "\(ProductArray[indexPath.row].specialPrice!)"
        cell.PriceLb.textColor = UIColor(hex: "\(ProductArray[indexPath.row].sp_pricecolor!)ff")
        cell.PrecentageLb.text = "\(ProductArray[indexPath.row].offerPercentage!) % Off"
        if ProductArray[indexPath.row].favorite == 0{
            cell.FavImage.image = UIImage(named: "favUnselected")
        }else{
            cell.FavImage.image = UIImage(named: "favSelected")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2.1
        let multiplier = width / 8
        let height = 10 * multiplier
        return CGSize(width: width, height: height + 65)
//        return CGSize(width: collectionView.frame.width / 2.1, height: 265)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .productDetailsRecentProducts
        MHProductDetailsVC.BasicData7 = ProductArray[indexPath.row]
        productID = ProductArray[indexPath.row].productId
         frompush = false
        self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
    }
    func rescentProductsFav(index: Int, cell: RescentProductsCVC){
        isUpdateRecent = true
        if currentProductId == ProductArray[index].productId{
            isDashboardUpdted = true
            updatedProduct = ProductArray[index].productId
        }
        var type:String = ""
        if ProductArray[index].favorite == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = [  "action":"Shortlist",
                            "userid":"\(UserID)",
            "guestId": guest_id,
            "productid": ProductArray[index].productId!,
            "type" : type] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                if self.ProductArray[index].favorite == 0{
                    self.ProductArray[index].favorite = 1
                }else{
                    self.ProductArray[index].favorite = 0
                }
                self.recentCV.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHRescentProductListVC{
    func rescentList(){
        if recentCV.refreshControl?.isRefreshing == true{
            print("Refreshing...")
                }else{
                    print("Calling API")
                }
        //https://www.masho.com/api_v4?action=getProductsByCatId&api_id=147&batchSize=20&guest_id=3157447&catsubcat=sc
        let parameters = [  "action":"recentlyviewed",
                            "userId":"\(UserID)",
                            "guest_id": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [[String:Any]]{
                    for item in data{
                        self.ProductArray.append(MHrecentlyViewedbodyModel(fromData: item))
                    }
                  
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5){
                                   self.recentCV.refreshControl?.endRefreshing()
                                  
                                   self.recentCV.reloadData()
                               }
                }
                
                self.recentCV.reloadData()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    
}
