//
//  MHOTPVC.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import IQKeyboardManagerSwift
class MHOTPVC: UIViewController {
    var seconds = 60
    var timer = Timer()
    var otp : Int?
    var isFromLogin = false
    var code : String?
    var OTPmessage : String?
    var Countrycode : String?
    var PhoneNumber : String?
    var txtFields : [OTPTextField]!
    let Device_token : String = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
    var guest_id : Int? = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var userDetails : VerificationModel!
    var CountryCode : String = "IN"
    var CountryName : String = "India"
    let locale = Locale.current
    var GuestData : guestModel! = nil
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var FirstTF: OTPTextField!
    @IBOutlet weak var SecondTF: OTPTextField!
    @IBOutlet weak var ThirdTF: OTPTextField!
    @IBOutlet weak var FourthTF: OTPTextField!
    @IBOutlet weak var FirstLine: UIView!
    @IBOutlet weak var SecondLine: UIView!
    @IBOutlet weak var ThirdLine: UIView!
    @IBOutlet weak var FourthLine: UIView!
    @IBOutlet weak var CountLb: UILabel!
    @IBOutlet weak var ResendBTN: UIButton!
    @IBOutlet weak var OTPmsgLb: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirstTF.textContentType = .oneTimeCode
        SecondTF.textContentType = .oneTimeCode
        ThirdTF.textContentType = .oneTimeCode
        FourthTF.textContentType = .oneTimeCode
        FirstTF.becomeFirstResponder()
        FirstTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        SecondTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        ThirdTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        FourthTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        OTPmsgLb.text = OTPmessage!
        scroll.scrollToBottom(animated: false)
        scroll.bounces = false
        initilizeUI()
    }
    var firstTime : Bool? = nil
    override func viewWillAppear(_ animated: Bool) {
        if firstTime != nil {
            FirstTF.resignFirstResponder()
            firstTime = false
        }
        SecondTF.resignFirstResponder()
        ThirdTF.resignFirstResponder()
        FourthTF.resignFirstResponder()
    }
    @objc func doneButtonClicked(_ sender: Any) {
        var f = self.view.frame
        f.origin.y = 0
        self.view.frame = f
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ResendOTPBTNtapped(_ sender: Any) {
        Login()
        FirstTF.text = ""
        SecondTF.text = ""
        ThirdTF.text = ""
        FourthTF.text = ""
        CountLb.textColor = UIColor.MHGold
        ResendBTN.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        ResendBTN.isUserInteractionEnabled = false
        seconds = 60
        self.runTimer()
        
    }
    var type : Int!
    @IBAction func VeriftOTPBTNtapped(_ sender: Any) {
        
        
        if FirstTF.text != "" && SecondTF.text != "" && ThirdTF.text != "" && FourthTF.text != ""{
            let StringOTP = FirstTF.text! + SecondTF.text! + ThirdTF.text! + FourthTF.text!
            let enteredOTP = Int(FirstTF.text! + SecondTF.text! + ThirdTF.text! + FourthTF.text!)
            if otp == enteredOTP{
                if isFromLogin{
                    if guest_id == 0 {
                        self.type = 0
//                        self.CountryCode = locale.regionCode!
//                        self.CountryName = countryName(from: locale.regionCode!)
                        guestlogin(otp: StringOTP)
                    }else{
                        VerifyOTP(otp: StringOTP)
                    }
                }else{
                    if guest_id == 0 {
                        self.type = 1
                        self.CountryCode = locale.regionCode!
                        self.CountryName = countryName(from: locale.regionCode!)
                        guestlogin(otp: StringOTP)
                    }else{
                        VerifyRegisterOTP(otp: StringOTP)
                    }
                    
                }
            }else{
                Banner.main.showBanner(title: "", subtitle: "Invalid OTP", style: .danger)
            }

        }else{
            Banner.main.showBanner(title: "", subtitle: "Enter your OTP to continue", style: .danger)
        }
        
    }
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    fileprivate func initilizeUI() {
        self.txtFields = [FirstTF,SecondTF,ThirdTF,FourthTF]
        FirstTF.delegate = self
        SecondTF.delegate = self
        ThirdTF.delegate = self
        FourthTF.delegate = self
        FirstTF.textFieldDelegate = self
        SecondTF.textFieldDelegate = self
        ThirdTF.textFieldDelegate = self
        FourthTF.textFieldDelegate = self
        FirstLine.backgroundColor = UIColor.lightGray
        SecondLine.backgroundColor = UIColor.lightGray
        ThirdLine.backgroundColor = UIColor.lightGray
        FourthLine.backgroundColor = UIColor.lightGray
        CountLb.textColor = UIColor.MHGold
        ResendBTN.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        ResendBTN.isUserInteractionEnabled = false
        self.runTimer()
    }
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        CountLb.text = "Resend code in 0:\(seconds)"
        if seconds == 0{
            timer.invalidate()
            CountLb.textColor = UIColor.white.withAlphaComponent(0.5)
            self.ResendBTN.alpha = 1
            ResendBTN.setTitleColor(UIColor.MHGold, for: .normal)
            ResendBTN.isUserInteractionEnabled = true
        }
    }


}
extension MHOTPVC : UITextFieldDelegate , OTPTFDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var f = self.view.frame
        if screenHeight / screenWidth < 2{
            f.origin.y = -265
        }else{
            f.origin.y = -345
        }
        self.view.frame = f
        DispatchQueue.main.async {
            self.txtFields.forEach { (TF) in
                if TF != textField {
                    switch TF {
                    case self.FirstTF:
                        self.FirstLine.backgroundColor = (TF.text?.count == 0 || TF.text == "") ? UIColor.lightGray : UIColor.MHGold
                    case self.SecondTF:
                        self.SecondLine.backgroundColor = (TF.text?.count == 0 || TF.text == "") ? UIColor.lightGray : UIColor.MHGold
                    case self.ThirdTF:
                        self.ThirdLine.backgroundColor = (TF.text?.count == 0 || TF.text == "") ? UIColor.lightGray : UIColor.MHGold
                    case self.FourthTF:
                        self.FourthLine.backgroundColor = (TF.text?.count == 0 || TF.text == "") ? UIColor.lightGray : UIColor.MHGold
                    default:
                        break
                    }
                } else {
                    switch textField {
                    case self.FirstTF:
                        self.FirstLine.backgroundColor = UIColor.MHGold
                    case self.SecondTF:
                        self.SecondLine.backgroundColor = UIColor.MHGold
                    case self.ThirdTF:
                        self.ThirdLine.backgroundColor = UIColor.MHGold
                    case self.FourthTF:
                        self.FourthLine.backgroundColor = UIColor.MHGold
                        
                    default:
                        break
                        
                    }
                }
            }
        }
    }
    
    
    func didTapBackspace(sender: OTPTextField) {
        switch sender {
        case SecondTF:
            self.SecondLine.backgroundColor = UIColor.lightGray
            FirstTF.becomeFirstResponder()
        case ThirdTF:
            self.ThirdLine.backgroundColor = UIColor.lightGray
            SecondTF.becomeFirstResponder()
        case FourthTF:
            self.FourthLine.backgroundColor = UIColor.lightGray
            ThirdTF.becomeFirstResponder()
        default:
            print("codeDigitOne")
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("replacing string :\(string)")
        textField.text = ""
        if ((textField.text?.count)! < 1) && (string.count > 0) {
            
            switch textField {
            case FirstTF:
                SecondTF.becomeFirstResponder()
            case SecondTF:
                ThirdTF.becomeFirstResponder()
            case ThirdTF:
                FourthTF.becomeFirstResponder()
            case FourthTF:
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.FourthTF.resignFirstResponder()
                    var f = self.view.frame
                    f.origin.y = 0
                    self.view.frame = f
                })
                return true
            default:
                break
            }
            
            textField.text = string
            return false
        }
        else if ((textField.text?.count)! >= 1) && (string.count == 0){
            switch textField {
            case SecondTF:
                FirstTF.becomeFirstResponder()
            case ThirdTF:
                SecondTF.becomeFirstResponder()
            case FourthTF:
                ThirdTF.becomeFirstResponder()
            default:
                break
            }
            textField.text = string
            return false
        }
        else if (textField.text?.count)! >= 1{
            switch textField {
            case FirstTF:
                SecondTF.becomeFirstResponder()
            case SecondTF:
                ThirdTF.becomeFirstResponder()
            case ThirdTF:
                FourthTF.becomeFirstResponder()
            case FourthTF:
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.FourthTF.resignFirstResponder()
                    var f = self.view.frame
                    f.origin.y = 0
                    self.view.frame = f
                })
                return true
            default:
                break
            }
            textField.text = string
            return false
        }
        return true
    }
    
}

protocol OTPTFDelegate : class {
    func didTapBackspace(sender: OTPTextField)
}

class OTPTextField : UITextField {
    
    weak var textFieldDelegate: OTPTFDelegate?
    override func deleteBackward() {
        let isTextfieldEmpty: Bool = (self.text?.isEmpty)!
        if isTextfieldEmpty {
            textFieldDelegate?.didTapBackspace(sender: self)
        }
        super.deleteBackward()
    }
    
}
extension MHOTPVC{
    func VerifyOTP(otp: String){
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action":"PostLogin",
                          "mobcode": self.code ?? "+91",
                          "mobile": self.PhoneNumber!,
                          "otp": otp,
                          "guestId": guest_id!,
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                if let data = response["data"] as? [String: Any]{
                    self.userDetails = VerificationModel(fromData: data)
                }
                isLogged = true
                isLanguageUpdated = true
                UserDefaults.standard.set("\(self.userDetails.userId!)", forKey: "userID")
                UserDefaults.standard.set("\(self.userDetails.userName!)", forKey: "userName")
                UserDefaults.standard.set("\(self.userDetails.device_tble_id!)", forKey: "tableID")
                UserDefaults.standard.set(0, forKey: "guestId")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
        
    }
    
    func VerifyRegisterOTP(otp: String){
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        let parameters = ["action":"checkotp_register",
                          "mobcode": self.code!,
                          "mobile": self.PhoneNumber!,
                          "otp": otp,
                          "guestId": guest_id!,
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                if let data = response["data"] as? [String: Any]{
                    self.userDetails = VerificationModel(fromData: data)
                }
                isLogged = true
                isLanguageUpdated = true
                UserDefaults.standard.set("\(self.userDetails.userId!)", forKey: "userID")
                UserDefaults.standard.set("\(self.userDetails.userName!)", forKey: "userName")
                UserDefaults.standard.set("\(self.userDetails.device_tble_id!)", forKey: "tableID")
                UserDefaults.standard.set(0, forKey: "guestId")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: false)
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func guestlogin(otp: String){
        let parameters = [  "action" : "guestlogin",
                            "countrycode" : self.CountryCode,
                            "countryname" : self.CountryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let guestData = response["data"] as? [String:Any]{
                    self.GuestData = guestModel(fromData: guestData)
                }
                UserDefaults.standard.set(self.GuestData.guestid, forKey: "guestId")
                if self.type == 0{
                    self.VerifyOTP(otp: otp)
                }else{
                    self.VerifyRegisterOTP(otp: otp)
                }
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    func Login(){
        let parameters = ["action":"checkmobilenumber",
                          "mobcode": self.code!,
                          "mobile": PhoneNumber!,
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        print(parameters)
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
