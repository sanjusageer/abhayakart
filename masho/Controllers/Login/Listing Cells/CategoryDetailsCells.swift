//
//  CategoryDetailsCells.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class CategoryCVC: UICollectionViewCell{
    
    @IBOutlet weak var CategoryIcon: UIImageView!
    @IBOutlet weak var categoryLb: UILabel!
    @IBOutlet weak var ShadowView: BaseView!
}


class CategoryProductCVC: UICollectionViewCell {
    var delegate: FavoriteProductDelegate!
    var index : Int!
    
    @IBOutlet weak var Productimage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var StrikePrice: UILabel!
    @IBOutlet weak var OfferPrice: UILabel!
    @IBOutlet weak var PercentageLb: UILabel!
    
    
    @IBOutlet weak var FavView: BaseView!
    @IBOutlet weak var FavImg: UIImageView!
    
    @IBAction func FavBTNtapped(_ sender: Any) {
//        let isLoggedIn = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
//        if isLoggedIn{
            delegate.didSelectFavorite(indx: index)
//        }else{
//            delegate.redirectToLogin()
//        }
    }
}

