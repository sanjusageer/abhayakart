//
//  MHCartVC.swift
//  masho
//
//  Created by Appzoc on 13/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import DropDown
import Firebase
var isFromCartCheckout = false
var cartData : MHCartmodel!
class MHCartVC: UIViewController {
    var cartProducts = ["","",""]
    var isFromProductDetails = false
    var UserID : String = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
    var guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var TableID : String = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
    var dropDown = DropDown()
    var index : Int!
    var dataSource = [String]()
    var selectedProductId : String!
    var categoryArray = [categoryListmodel]()
    var CartItemData : MHpopupModel!
    @IBOutlet weak var HideView2: UIView!
    @IBOutlet weak var HideView1: UIView!
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var EmptyView: UIView!
    @IBOutlet weak var ContinueShoppingView: UIView!
    @IBOutlet weak var ContinueShoppingLb: UILabel!
    @IBOutlet weak var CheckOutView: UIView!
    @IBOutlet weak var CheckOutLb: UILabel!
    
    @IBOutlet weak var DeleteProductConfirmedBTN: UIButton!
    @IBOutlet weak var ConfirmationPopupView: UIView!
    @IBOutlet weak var SubQuantityView: BaseView!
    @IBOutlet weak var QtyCV: UICollectionView!
    @IBOutlet weak var QuantityView: UIView!
    @IBOutlet weak var CartTV: UITableView!
    
    @IBOutlet weak var MainPopUpView:UIView!
    @IBOutlet weak var PopUpMainTitle: UILabel!
    @IBOutlet weak var PopUpSubTitle: UILabel!
    @IBOutlet weak var PopUpBTNView:UIView!
    @IBOutlet weak var PopUpBTNTitle: UILabel!
    
    var checkouturl : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        QuantityView.isHidden = true

        setDropdown()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCartItems()
        logEvent()
//        if isFromCartCheckout{
//            isFromCartCheckout = false
//            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = self.cartData.checkouturl
//            self.navigationController?.pushViewController(WebViewVC, animated: true)
//        }
        
    }
    func logEvent(){
        Analytics.logEvent("Cart_Opened", parameters: [:])
    }
    
    @IBAction func ClosePopUpView(_ sender: Any) {
        self.MainPopUpView.isHidden = true
    }
    @IBAction func ContinueShoppingInPopUpView(_ sender: Any) {
        self.MainPopUpView.isHidden = true
        if cartData.popup.apiname == "getProductsByCatId"{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
            vc.categoryId = "\(cartData.popup.api_id!)"
            vc.SubCategoryId = "sc"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if cartData.popup.apiname == "homepagedetails"{
            let navigationStack = navigationController?.viewControllers ?? []
            for vc in navigationStack{
                if let dashboardVC = vc as? MHdashBoardVC{
                    DispatchQueue.main.async {
                        self.navigationController?.popToViewController(dashboardVC, animated: true)
                    }
                }
            }
        }else if cartData.popup.apiname == "getProductDetails"{
            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            MHProductDetailsVC.isFrom = .other
//            MHProductDetailsVC.productID = cartData.popup.api_id
            productID = cartData.popup.api_id
             frompush = false
            self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
        }else if cartData.popup.apiname == "getCartItems" {
            let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                CartVC.categoryArray = self.categoryListArray
            frompush = false
            self.navigationController?.pushViewController(CartVC, animated: true)
        }
    }
    
    @IBAction func ContinueShoppingBTNtapped(_ sender: Any) {
        let navigationStack = navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: true)
                }
            }
        }
        
    }
    
    
    @IBAction func CheckOutBTNtapped(_ sender: Any) {
        if cartData.popup.popup != 0{
            self.PopUpMainTitle.text = cartData.popup.message
            self.PopUpSubTitle.text = cartData.popup.message2
            self.PopUpBTNView.backgroundColor = UIColor(hex: "\(cartData.popup.button.background ?? "#ffffff")ff")
            self.PopUpBTNTitle.text = cartData.popup.button.text
            self.MainPopUpView.isHidden = false
        }else{
            self.MainPopUpView.isHidden = true
            if UserID == ""{
                isFromCartCheckout = true
                let loginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
                self.navigationController?.pushViewController(loginVC, animated: true)
            }else{
    //            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
    //            WebViewVC.WebURL = self.cartData.checkouturl
    //            WebViewVC.isFromCart = true
                let WebViewVC = StoryBoard.main.instantiateViewController(withIdentifier: "MHBaseViewVC") as! MHBaseViewVC
                self.navigationController?.pushViewController(WebViewVC, animated: true)
            }
        }
        
        
    }
    
    @IBAction func PerformTap(_ sender: UITapGestureRecognizer) {
        QuantityView.isHidden = true
    }
    
    @IBAction func RemoveProductFromCartConfirmedBTNtapped(_ sender: UIButton) {
        QuantityView.isHidden = true
        removeCartItems(Product_Id: cartData.cartItems[sender.tag].productId)
    }
    @IBAction func cancelProductFromCartConfirmedBTNtapped(_ sender: UIButton) {
        QuantityView.isHidden = true
    }
    @IBAction func RemoveProductFromCartBTNtapped(_ sender: UIButton) {
        QuantityView.isHidden = false
        SubQuantityView.isHidden = true
        ConfirmationPopupView.isHidden = false
        self.DeleteProductConfirmedBTN.tag = sender.tag
    }
    func dropDownSetup(anchorView: UIView,dataSource: [String],cell: MHCartProductTVC,indx : Int,productId : String){
        dropDown.anchorView = anchorView
        dropDown.dataSource = dataSource
        DropDown.appearance().cornerRadius = 4
//      DropDown.appearance().backgroundColor = #colorLiteral(red: 0.8306156993, green: 0.6888336539, blue: 0.2119885683, alpha: 1)
        dropDown.direction = .bottom
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 13)!
        dropDown.dismissMode = .onTap
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            self.addCartItems(Product_Id: productId, quantity: item, cell: cell)
        }
    }
   
    @IBAction func BackBTNtapped(_ sender: Any) {
        if isFromProductDetails{
            let navigationStack = navigationController?.viewControllers ?? []
            for vc in navigationStack{
                if let productDetailsVC = vc as? MHProductDetailsVC{
                    DispatchQueue.main.async {
                        self.navigationController?.popToViewController(productDetailsVC, animated: true)
                    }
                }
            }
        }
        else if frompush {
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
        
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
    }
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }
            else if DropDownData[index].apiname == "Categories"{
//                self.ColorArray.shuffle()
//                vc.colorArray = self.ColorArray
    //                WebViewVC.WebURL = SideMenuArray[indexPath.row].api
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
                
                else {
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
extension MHCartVC: UITableViewDelegate,UITableViewDataSource,showDropdownDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            if cartData != nil{
                if cartData.cartItems.count == 0{
                    return 1
                }else{
                    return cartData.cartItems.count
                }
            }else{
                return 1
            }
            
        }else if section == 2{
            return 1
        }else if section == 3{
            return 1
        }else{
            if cartData != nil{
                return cartData.OrderSummary.count
            }
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartFirstCell", for: indexPath) as! CartFirstCell
            if cartData != nil{
                cell.TextLb.text = cartData.labels.secondtext.text
                cell.BackView.backgroundColor = UIColor(hex: "\(cartData.labels.secondtext.background!)ff")
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHCartProductTVC", for: indexPath) as! MHCartProductTVC
            if cartData != nil{
                if cartData.cartItems.count == 0{
                    cell.EmptyView.isHidden = false
                    cell.TextLb.text = "Sorry !!! Your bag is empty"
                    cell.BorderLine.isHidden = true
                }else{
                    cell.EmptyView.isHidden = true
                    cell.delegate = self
                    cell.index = indexPath.row
                    cell.RemoveBTN.tag = indexPath.row
                    cell.ProductImage.kf.setImage(with: URL(string: cartData.cartItems[indexPath.row].imageUrl))
                    cell.qtyPriceLb.text = "\(cartData.cartItems[indexPath.row].qty ?? "") x \(cartData.cartItems[indexPath.row].price ?? "") = "
                    cell.PriceLb.text = cartData.cartItems[indexPath.row].totalitemprice
                    cell.ProductName.text = cartData.cartItems[indexPath.row].productTitle
                    cell.ProductTitle.text = cartData.cartItems[indexPath.row].productName
                    cell.QuantityLb.text = cartData.cartItems[indexPath.row].qty
                    DispatchQueue.main.async{
                        if indexPath.row == cartData.cartItems.count - 1{
                            cell.BorderLine.isHidden = true
                        }else{
                            cell.BorderLine.isHidden = false
                        }
                    }
                }
            }
            
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartSecondCell", for: indexPath) as! CartSecondCell
            if cartData != nil{
                cell.TextLb.text = cartData.labels.thirdtext
            }
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartThirdCell", for: indexPath) as! CartThirdCell
            if cartData != nil{
                cell.TextLb1.text = cartData.labels.textfour
//                cell.TextLb2.text = cartData.labels.textfive
//                if cartData.cartItems.count == 0{
//                    cell.WhiteView.isHidden = true
//                }else{
//                    cell.WhiteView.isHidden = false
//                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MHOrderSummaryTVC", for: indexPath) as! MHOrderSummaryTVC
            if cartData != nil{
                cell.FirstLb.text = cartData.OrderSummary[indexPath.row].text
                cell.SubTxtLb.text = cartData.OrderSummary[indexPath.row].subtext
                if cartData.OrderSummary[indexPath.row].style != nil{
                    cell.BackView.backgroundColor = UIColor(hex: "\(cartData.OrderSummary[indexPath.row].style.background ?? "")ff")
                    cell.FirstLb.textColor = UIColor.black
                    cell.SubTxtLb.textColor = UIColor(hex: "\(cartData.OrderSummary[indexPath.row].style.color ?? "")ff")
                    cell.PriceLb.textColor = UIColor.black
                }else{
                    cell.BackView.backgroundColor = UIColor.white
                    cell.FirstLb.textColor = UIColor.black
                    cell.SubTxtLb.textColor = UIColor.lightGray
                    cell.PriceLb.textColor = UIColor.black
                }
                
                if cartData.OrderSummary[indexPath.row].val != nil{
                    cell.PriceLb.text = "\(cartData.OrderSummary[indexPath.row].val ?? "")"
                }
                
                if cartData.OrderSummary[indexPath.row].text == "shipping Charge" && cartData.OrderSummary[indexPath.row].val == nil || cartData.OrderSummary[indexPath.row].val == "0"{
                    cell.PriceLb.text = "Free"
                }
                if cartData.OrderSummary[indexPath.row].text == "Total Amount"{
//                    cell.FirstLb.text = "Amount Payable"
                    cell.FirstLb.font = UIFont(name: "Roboto-Medium", size: 16)
                    cell.PriceLb.font = UIFont(name: "Roboto-Bold", size: 15)
                }else{
                    cell.FirstLb.font = UIFont(name: "Roboto-Regular", size: 13)
                    cell.PriceLb.font = UIFont(name: "Roboto-Regular", size: 13)
                }
                
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 50
        }else if indexPath.section == 1{
            return 168
        }else if indexPath.section == 2{
            return 60
        }else if indexPath.section == 3{
            return 120
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if cartData.cartItems.count != 0{
                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                MHProductDetailsVC.isFrom = .cart
                MHProductDetailsVC.BasicData5 = cartData.cartItems[indexPath.row]
//                MHProductDetailsVC.productID = cartData.cartItems[indexPath.row].productId
                productID = cartData.cartItems[indexPath.row].productId
                 frompush = false
                self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
            }
            
        }
    }
    func showDropdown(cell: MHCartProductTVC,index: Int){
        
        if cartData.cartItems[index].avalablestock < 5{
            for i in 1 ... cartData.cartItems[index].avalablestock{
                dataSource.append("\(i)")
            }
        }else{
            dataSource = ["1","2","3","4","5"]
        }
        print(dataSource)
        self.index = dataSource.firstIndex(where: {$0 == cartData.cartItems[index].qty})
        self.selectedProductId = cartData.cartItems[index].productId
        QuantityView.isHidden = false
        SubQuantityView.isHidden = false
        ConfirmationPopupView.isHidden = true
        self.QtyCV.reloadData()
//        self.dropDownSetup(anchorView: cell.anchorView,dataSource: dataSource,cell: cell, indx : index,productId : cartData.cartItems[index].productId)
//        dropDown.show()
    }
}
extension MHCartVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuantityCVC", for: indexPath) as! QuantityCVC
        cell.QtyLb.text = dataSource[indexPath.row]
        DispatchQueue.main.async {
            cell.CircleView.layer.cornerRadius = cell.CircleView.frame.height / 2
        }
        if index == indexPath.row{
            cell.CircleView.backgroundColor = UIColor.MHGold
            cell.CircleView.layer.borderColor = UIColor.MHGold.cgColor
            cell.QtyLb.textColor = UIColor.white
        }else{
            cell.CircleView.backgroundColor = UIColor.white
            cell.CircleView.layer.borderColor = UIColor.black.withAlphaComponent(0.25).cgColor
            cell.QtyLb.textColor = UIColor.lightGray
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 5, height: collectionView.frame.width / 5)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        collectionView.reloadData()
        QuantityView.isHidden = true
        self.addCartItems(Product_Id: self.selectedProductId, quantity: dataSource[index])
        print(indexPath.row)
       
    }
}
extension MHCartVC{
    func getCartItems(){
        //https://www.masho.com/api/?action=getCartItems&guest_id=&userId=732&tableid=U9ZHI3QW0EARuNy
        
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = ["action":"getCartItems",
                          "userId":"\(UserID)",
                          "guest_id": guest_id,
                          "tableid": TableID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                //https://www.masho.com/api?action=getCartItems&userid=813&guest_id=47603
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    cartData = MHCartmodel(fromData: data)
                }
                UserDefaults.standard.setValue(cartData.cartItems.count, forKey: "CartItems")
                self.ContinueShoppingView.backgroundColor = UIColor(hex: "\(cartData.labels.buttonone.background!)ff")
             self.ContinueShoppingLb.text = "CONTINUE SHOPPING"
               // cartData.labels.buttonone.text
                self.ContinueShoppingLb.textColor = UIColor(hex: "\(cartData.labels.buttonone.color!)ff")
                self.CheckOutView.backgroundColor = UIColor(hex: "\(cartData.labels.buttontwo.background!)ff")
                self.CheckOutLb.text = "CHECKOUT"
//                    cartData.labels.buttontwo.text
                self.CheckOutLb.textColor = UIColor(hex: "\(cartData.labels.buttontwo.color!)ff")
                if cartData.cartItems.count == 0{
                    self.CartTV.isHidden = true
                    self.EmptyView.isHidden = false
                    self.HideView1.isHidden = true
                    self.HideView2.isHidden = true
                }else{
                    self.CartTV.isHidden = false
                    self.EmptyView.isHidden = true
                    self.HideView1.isHidden = false
                    self.HideView2.isHidden = false
                }
                
                if cartData.popup.popup != 0{
                    self.PopUpMainTitle.text = cartData.popup.message
                    self.PopUpSubTitle.text = cartData.popup.message2
                    self.PopUpBTNView.backgroundColor = UIColor(hex: "\(cartData.popup.button.background ?? "#ffffff")ff")
                    self.PopUpBTNTitle.text = cartData.popup.button.text
                    self.PopUpBTNTitle.textColor = UIColor(hex: "\(cartData.popup.button.color ?? "#ffffff")ff")
                }
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                self.CartTV.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHCartVC{
    func addCartItems(Product_Id: String, quantity: String){
        //https://www.masho.com/api?action=cartiemcount&guest_Id=0&productId=3012&qty=2&userId=337
        
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = [  "action":"cartiemcount",
                            "userId":"\(UserID)",
                            "guest_Id": guest_id,
                            "productId":Product_Id,
                            "qty":quantity]         as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                self.getCartItems()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.CartItemData = MHpopupModel(fromData: data)
                }
                if self.CartItemData != nil{
                    if self.CartItemData.popup != 0{
                        self.PopUpMainTitle.text = self.CartItemData.message
                        self.PopUpSubTitle.text = self.CartItemData.message2
                        self.PopUpBTNView.backgroundColor = UIColor(hex: "\(self.CartItemData.button.background ?? "#ffffff")ff")
                        self.PopUpBTNTitle.text = self.CartItemData.button.text
                        self.PopUpBTNTitle.textColor = UIColor(hex: "\(self.CartItemData.button.color ?? "#ffffff")ff")
                        self.MainPopUpView.isHidden = false
                    }else{
                        self.MainPopUpView.isHidden = true
                    }
                }
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension MHCartVC{
    func removeCartItems(Product_Id: String){
        
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = [  "action":"removeItem",
                            "userId":"\(UserID)",
                            "guest_id": guest_id,
                            "productId":Product_Id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                self.getCartItems()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
//extension MHCartVC{
//    func checkOut(){
//
//        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
//        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
//        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
//        let parameters = [  "action":"checkout",
//                            "userId":"\(UserID)",
//                            "guest_id": guest_id,
//                            "tableid":TableID] as [String : Any]
//        self.view.alpha = 0.5
//        self.view.isUserInteractionEnabled = false
//        KVSpinnerView.show()
//        NetworkManager.webcallWithErrorCode(urlString: "https://www.masho.com/api/", methodeType: .post,parameter: parameters) { (status, response) in
//            print(response)
//            switch status {
//            case .noNetwork:
//                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
//            case .success :
//
//                print("success")
//                print(response["data"]!)
//                if let data = response["data"] as? [String:Any]{
//                    self.checkouturl = data["checkouturl"] as? String
//                }
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//                WebViewVC.WebURL = self.checkouturl
//                self.navigationController?.pushViewController(WebViewVC, animated: true)
//            case .failure :
//                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
//            case .unknown:
//                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
//                KVSpinnerView.dismiss()
//                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
//            }
//        }
//    }
//}
