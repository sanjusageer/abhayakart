//
//  DashboardModels.swift
//  masho
//
//  Created by Appzoc on 28/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class welcomeModel{
    var image : String!
    var welcome_message : String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.welcome_message = data["welcome_message"] as? String
    }
}

class SideMenuModel {
    var image : String!
    var leftmenuname : String!
    var api : String!
    var api_id : Int!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.type = data["type"] as? Int
        self.leftmenuname = data["leftmenuname"] as? String
        self.api = data["api"] as? String
        self.api_id = data["api_id"] as? Int
    }
    deinit {
        debugPrint("deinit SideMenuModel")
    }
}
class SideMenucountrydetailsModel{
    var countryname : String!
    var Language : String!
    var languageid : Int!
    var Currency : String!
    var Currencyid : String!
    var image : String!
    var countryid : String!
    var countryselect : SideMenuCountrySelectModel!
    var languageselect : SideMenuLanguageSelectModel!
    var Currencyselect : SideMenuCurrencySelectModel!
    init(fromData data: [String:Any]) {
        self.countryname = data["countryname"] as? String
        self.languageid = data["languageid"] as? Int
        self.Currencyid = data["Currencyid"] as? String
        self.Language = data["Language"] as? String
        self.Currency = data["Currency"] as? String
        self.image = data["image"] as? String
        self.countryid = data["countryid"] as? String
        if let countryselectData = data["countryselect"] as? [String : Any]{
            self.countryselect = SideMenuCountrySelectModel(fromData: countryselectData)
        }
        if let languageselectData = data["languageselect"] as? [String : Any]{
            self.languageselect = SideMenuLanguageSelectModel(fromData: languageselectData)
        }
        if let CurrencyselectData = data["Currencyselect"] as? [String : Any]{
            self.Currencyselect = SideMenuCurrencySelectModel(fromData: CurrencyselectData)
        }
    }
}
class SideMenuLanguageSelectModel{
    var text : String!
    var datas = [SideMenuLanguageSelectDataModel]()
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        if let dataList = data["datas"] as? [[String : Any]]{
            datas = [SideMenuLanguageSelectDataModel]()
            for item in dataList{
                self.datas.append(SideMenuLanguageSelectDataModel(fromData: item))
            }
        }
    }
}
class SideMenuLanguageSelectDataModel{
    var value : String!
    var text : String!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.value = data["value"] as? String
    }
}
class SideMenuCurrencySelectModel{
    var text : String!
    var datas = [SideMenuCurrencySelectDataModel]()
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        if let dataList = data["datas"] as? [[String : Any]]{
            datas = [SideMenuCurrencySelectDataModel]()
            for item in dataList{
                self.datas.append(SideMenuCurrencySelectDataModel(fromData: item))
            }
        }
    }
}
class SideMenuCurrencySelectDataModel{
    var value : String!
    var text : String!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.value = data["value"] as? String
    }
}
class SideMenuSettingsModel{
    var image : String!
    var name : String!
    var link : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.link = data["link"] as? String
        self.type = data["type"] as? Int
    }
}
class SideMenuContactUsModel{
    var image : String!
    var name : String!
    var link : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.link = data["link"] as? String
        self.type = data["type"] as? Int
    }
}
class SideMenuCountrySelectModel{
    var text : String!
    var datas = [SideMenuCountrySelectDatasModel]()
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        if let dataList = data["datas"] as? [[String : Any]]{
            datas = [SideMenuCountrySelectDatasModel]()
            for item in dataList{
                self.datas.append(SideMenuCountrySelectDatasModel(fromData: item))
            }
        }
    }
}
class SideMenuCountrySelectDatasModel{
    var text : String!
    var value : String!
    var image : String!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.value = data["value"] as? String
        self.image = data["image"] as? String
    }
}
class bannerImagesModel{
    var api_id : Int!
    var apiname : String!
    var image : String!
    var link : String!
    var type : Int!
    var catsubcat: String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.api_id = data["api_id"] as? Int
        self.apiname = data["apiname"] as? String
        self.link = data["link"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.type = data["type"] as? Int
    }
}
//class bannerImagesBottomModel{
//    var api_id : Int!
//    var apiname : String!
//    var image : String!
//    var link : String!
//    var type : Int!
//    init(fromData data: [String:Any]) {
//        self.image = data["image"] as? String
//        self.api_id = data["api_id"] as? Int
//        self.apiname = data["apiname"] as? String
//        self.link = data["link"] as? String
//        self.type = data["type"] as? Int
//    }
//}
class categoryListmodel{
    var image : String!
    var name : String!
    var apiname : String!
    var api_id : Int!
    var apiid : String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.apiname = data["apiname"] as? String
        if let id  = data["api_id"] as? Int {
            self.api_id = id
            print(name,api_id)
        } else {
            let id  = data["api_id"] as? String
            self.apiid = id
        }
    }
    deinit {
        debugPrint("deinit categoryListmodel")
    }
}

class categorymodel{
    var image : String!
    var name : String!
    var apiname : String!
    var api_id : Int!
    var type : Int!
    var products = [ProductModel]()
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.apiname = data["apiname"] as? String
        self.api_id = data["api_id"] as? Int
        self.type = data["type"] as? Int
        if let CategoryProducts = data["products"] as? [[String:Any]]{
            products = [ProductModel]()
            for item in CategoryProducts{
                self.products.append(ProductModel(fromData: item))
            }
        }
    }
}


class ProductModel{
    var imagetitle : String!
    var image : String!
    var productname : String!
    var strikeprice : String!
    var specialprice : String!
    var productlink : String!
    var shortlist:String!
    var productID:String!
    var offerPercentage:Int!
    var deliveryMode:String!
    var mashoAssured:Int!
    var paymentMode:String!
    var rating:String!
    var returnPolicy:String!
    var shipping:String!
    var type:String!
    init(fromData data: [String:Any]) {
        self.imagetitle = data["imagetitle"] as? String
        self.image = data["image"] as? String
        self.productname = data["productname"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.specialprice = data["specialprice"] as? String
        self.productlink = data["productlink"] as? String
        self.shortlist = "\(data["shortlist"] as? Int ?? 0)"
        self.productID = "\(data["productid"] as? String ?? "0")"
        self.offerPercentage =  data["offerPercentage"] as? Int
        self.deliveryMode = data["deliveryMode"] as? String
        self.mashoAssured =  data["mashoAssured"] as? Int
        self.paymentMode = data["paymentMode"] as? String
        self.rating =  data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type =  data["type"] as? String
    }
}

class CategoryProductModel: Equatable {
    static func == (lhs: CategoryProductModel, rhs: CategoryProductModel) -> Bool {
        return lhs.productID == rhs.productID
    }
    
    var imagetitle : String!
    var image : String!
    var productname : String!
    var strikeprice : String!
    var specialprice : String!
    var productlink : String!
    var shortlist:String!
    var productID:String!
    var offerpercent:Int!
    var deliveryMode:String!
    var mashoAssured:String!
    var paymentMode:String!
    var rating:String!
    var returnPolicy:String!
    var shipping:String!
    var type:String!
    init(fromData data: [String:Any]) {
        self.imagetitle = data["imagetitle"] as? String
        self.image = data["image"] as? String
        self.productname = data["productname"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.specialprice = data["specialprice"] as? String
        self.productlink = data["productlink"] as? String
        self.shortlist = "\(data["shortlist"] as? Int ?? 0)"
        self.productID = "\(data["productid"] as? String ?? "0")"
        self.offerpercent =  data["offerpercent"] as? Int
        self.deliveryMode = data["deliveryMode"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type = data["type"] as? String
    }
}

class recentlyviewedModel{
    var data = [recentlyviewedProductModel]()
    var display : Int!
    var text : String!
    init(fromData data: [String:Any]) {
        self.display = data["display"] as? Int
        self.text = data["text"] as? String
        if let dataList = data["data"] as? [[String:Any]]{
            self.data = [recentlyviewedProductModel]()
            for item in dataList{
                self.data.append(recentlyviewedProductModel(fromData: item))
            }
        }
    }
}
class recentlyviewedProductModel{
    var assured : Int!
    var buttonvalue : String!
    var favorite : Int!
    var imageUrl : String!
    var offerPercentage : Int!
    var offerbackground : String!
    var offercolor : String!
    var price : String!
    var pricecolor : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var sp_pricecolor : String!
    var specialPrice : String!
    var deliveryMode : String!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var type : String!
    init(fromData data: [String:Any]) {
        self.assured = data["assured"] as? Int
        self.buttonvalue = data["buttonvalue"] as? String
        self.favorite = data["favorite"] as? Int
        self.imageUrl =  data["imageUrl"] as? String
        self.offerPercentage =  data["offerPercentage"] as? Int
        self.offerbackground =  data["offerbackground"] as? String
        self.offercolor =  data["offercolor"] as? String
        self.price =  data["price"] as? String
        self.pricecolor =  data["pricecolor"] as? String
        self.productId =  data["productId"] as? String
        self.productName =  data["productName"] as? String
        self.productTitle =  data["productTitle"] as? String
        self.sp_pricecolor =  data["sp_pricecolor"] as? String
        self.specialPrice =  data["specialPrice"] as? String
        self.deliveryMode =  data["deliveryMode"] as? String
        self.paymentMode =  data["paymentMode"] as? String
        self.rating =  data["rating"] as? String
        self.returnPolicy =  data["returnPolicy"] as? String
        self.shipping =  data["shipping"] as? String
        self.type =  data["type"] as? String
    }

}
class freshArrivalsModel{
    var api_id : String!
    var apiname : String!
    var productname : String!
    var strikeprice : String!
    var specialprice : String!
    var favorite : Int!
    var offerpercent : Int!
    var mainimage : String!
    var Title : String!
    var backgroundcolor : String!
    var deliveryMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var paymentMode : String!
    var mashoAssured : String!
    var subimages = [freshArrivalsSubImagesModel]()
    init(fromData data: [String:Any]) {
        self.backgroundcolor = data["backgroundcolor"] as? String
        self.api_id = data["api_id"] as? String
        self.apiname = data["apiname"] as? String
        self.productname = data["productname"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.specialprice = data["specialprice"] as? String
        self.favorite = data["favorite"] as? Int
        self.offerpercent = data["offerpercent"] as? Int
        self.mainimage = data["mainimage"] as? String
        self.Title = data["Title"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
        if let subimagesList = data["subimages"] as? [[String : Any]]{
            subimages = [freshArrivalsSubImagesModel]()
            for subimage in subimagesList{
                self.subimages.append(freshArrivalsSubImagesModel(fromData: subimage))
            }
        }
    }
}
class freshArrivalsSubImagesModel{
    var image : String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
    }
}
