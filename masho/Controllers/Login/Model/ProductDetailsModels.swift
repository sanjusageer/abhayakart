//
//  ProductDetailsCells.swift
//  masho
//
//  Created by Appzoc on 06/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class getProductsmodel{
    var currentPage : String!
    var batchSize : String!
    var lastPage : Int!
    init(fromData data: [String:Any]) {
        self.currentPage = data["currentPage"] as? String
        self.batchSize = data["batchSize"] as? String
        self.lastPage = data["lastPage"] as? Int
    }
}

class guestModel{
    var guestid : Int!
    init(fromData data: [String:Any]) {
        self.guestid = data["guestid"] as? Int
    }
}
class MHpopupModel{
    var api_id : String!
    var apiname : String!
    var button : MHpopupButtonModel!
    var message : String!
    var message2 : String!
    var popup : Int!
    init(fromData data: [String:Any]) {
        api_id = data["api_id"] as? String
        apiname = data["apiname"] as? String
        if let buttonData = data["button"] as? [String:Any]{
            self.button = MHpopupButtonModel(fromData: buttonData)
        }
        message = data["message"] as? String
        message2 = data["message2"] as? String
        popup = data["popup"] as? Int
    }
}
class MHpopupButtonModel{
    var background : String!
    var color : String!
    var text : String!
    init(fromData data: [String:Any]) {
        background = data["background"] as? String
        color = data["color"] as? String
        text = data["text"] as? String
    }
}
class size_shippingModel{
    var shipping_return : size_shipping_shipping_returnModel!
    var sizechart : size_shipping_sizechartModel!
    init(fromData data: [String:Any]) {
        if let data = data["shipping_return"] as? [String:Any]{
            self.shipping_return = size_shipping_shipping_returnModel(fromData: data)
        }
        if let data = data["sizechart"] as? [String:Any]{
            self.sizechart = size_shipping_sizechartModel(fromData: data)
        }
    }
}
class size_shipping_shipping_returnModel{
    var background : String!
    var color : String!
    var image : String!
    var status : Int!
    var text : String!
    var weburl : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.image = data["image"] as? String
        self.text = data["text"] as? String
        self.weburl = data["weburl"] as? String
        self.status = data["status"] as? Int
        
    }
}
class size_shipping_sizechartModel{
    var background : String!
    var color : String!
    var image : String!
    var status : Int!
    var text : String!
    var weburl : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.image = data["image"] as? String
        self.text = data["text"] as? String
        self.weburl = data["weburl"] as? String
        self.status = data["status"] as? Int
        
    }
}
class checkout_deliveryModel{
    var  image : String!
    var  status : Int!
    init(fromData data: [String:Any]) {
       
        self.status = data["status"] as? Int
        self.image = data["image"] as? String
       
        
    }
}
class MHProductDetailsModel{
    var ItemImageList = [MHItemImageListModel]()
    var availableColors = [MHavailableColorsModel]()
    var availableSizes = [MHavailableSizesModel]()
    var selectedproductid : String?
    var availableheight = [MHavailableheightModel]()
    var commonbuttoncolor : String!
    var popup : MHpopupModel!
    var postfields = [MHpostfieldsModel]()
    var productdetails = MHproductdetailzModel()
    var ratingdiv : Int!
    var recentlyViewed : MHrecentlyViewedModel!
    var specification = [MHspecificationModel]()
    var type : Int!
    var checkout_delivery : checkout_deliveryModel!
    var size_shipping : size_shippingModel!
    
    init(fromData data: [String:Any]) {
        self.commonbuttoncolor = data["commonbuttoncolor"] as? String
        self.selectedproductid = data["selectedproductid"] as? String
       // self.checkout_delivery = data["checkout_delivery"] as? String
        self.ratingdiv = data["ratingdiv"] as? Int
        self.type = data["type"] as? Int
        if let recentList = data["recentlyViewed"] as? [String:Any]{
            self.recentlyViewed = MHrecentlyViewedModel(fromData: recentList)
        }
        
        if let recentList = data["checkout_delivery"] as? [String:Any]{
            self.checkout_delivery = checkout_deliveryModel(fromData: recentList)
        }
        if let recentList = data["size_shipping"] as? [String:Any]{
            self.size_shipping = size_shippingModel(fromData: recentList)
        }
        if let PopUpData = data["popup"] as? [String:Any]{
            self.popup = MHpopupModel(fromData: PopUpData)
        }
        if let productdetailList = data["productdetails"] as? [String:Any]{
            self.productdetails = MHproductdetailzModel(fromData: productdetailList)
        }
        if let ImageList = data["ItemImageList"] as? [[String:Any]]{
            self.ItemImageList = [MHItemImageListModel]()
            for item in ImageList{
                self.ItemImageList.append(MHItemImageListModel(fromData: item))
            }
        }
        if let ColorList = data["availableColors"] as? [[String:Any]]{
            self.availableColors = [MHavailableColorsModel]()
            for item in ColorList{
                self.availableColors.append(MHavailableColorsModel(fromData: item))
            }
        }
        if let SizeList = data["availableSizes"] as? [[String:Any]]{
            self.availableSizes = [MHavailableSizesModel]()
            for item in SizeList{
                self.availableSizes.append(MHavailableSizesModel(fromData: item))
            }
        }
        if let heightList = data["availableheight"] as? [[String:Any]]{
            self.availableheight = [MHavailableheightModel]()
            for item in heightList{
                self.availableheight.append(MHavailableheightModel(fromData: item))
            }
        }
        if let postfieldsList = data["postfields"] as? [[String:Any]]{
            self.postfields = [MHpostfieldsModel]()
            for item in postfieldsList{
                self.postfields.append(MHpostfieldsModel(fromData: item))
            }
        }
        if let specificationList = data["specification"] as? [[String:Any]]{
            self.specification = [MHspecificationModel]()
            for item in specificationList{
                self.specification.append(MHspecificationModel(fromData: item))
            }
        }

    }
}
class MHImageOverviewModel{
    var imageUrl : String!
    var imageId : Int!
    init(fromData data: [String:Any]) {
        self.imageUrl = data["imageUrl"] as? String
        self.imageId = data["imageId"] as? Int
    }
}
class MHItemImageListModel{
    var imageUrl : String!
    var resolution : Int!
    var imageId : Int!
    var thumpnail : String!
    var autoplay :Int!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.imageUrl = data["imageUrl"] as? String
        self.resolution = data["resolution"] as? Int
        self.imageId = data["imageId"] as? Int
        self.thumpnail = data["thumpnail"] as? String
        self.autoplay = data["autoplay"] as? Int
        self.type = data["type"] as? Int
    }
}

class MHavailableColorsModel{
    var colorId : String!
    var colorImageURL : String!
    var colorName : String!
    var productlink : String!
    init(fromData data: [String:Any]) {
        self.colorImageURL = data["colorImageURL"] as? String
        self.colorId = data["colorId"] as? String
        self.colorName = data["colorName"] as? String
        self.productlink = data["productlink"] as? String
    }
}
class MHavailableSizesModel{
    var size : String!
    var sizeId : String!
    init(fromData data: [String:Any]) {
        self.sizeId = data["sizeId"] as? String
        self.size = data["size"] as? String
    }
}
class MHavailableheightModel{
    var size : String!
    var sizeId : Int!
    init(fromData data: [String:Any]) {
        self.sizeId = data["sizeId"] as? Int
        self.size = data["size"] as? String
    }
}
class MHpostfieldsModel{
    var field : String!
    var fieldname : String!
    var label : String!
    var options = [MHpostfieldsOptionsModel]()
    var placeholder : String!
    var required : Bool!
    var validationtxt : String!
    var value : String!
    init(fromData data: [String:Any]) {
        self.value = data["value"] as? String
        self.field = data["field"] as? String
        self.fieldname = data["fieldname"] as? String
        self.label = data["label"] as? String
        self.placeholder = data["placeholder"] as? String
        self.required = data["required"] as? Bool
        self.validationtxt = data["validationtxt"] as? String
        if let optionsList = data["options"] as? [[String:Any]]{
            options = [MHpostfieldsOptionsModel]()
            for item in optionsList{
                self.options.append(MHpostfieldsOptionsModel(fromData: item))
            }
        }
    }
}
class MHpostfieldsOptionsModel{
    var sizeId : String!
    var size : String!
    init(fromData data: [String:Any]) {
        self.sizeId = data["sizeId"] as? String
        self.size = data["size"] as? String
    }
}
class MHProductNewChangeModel{
    var background : String!
    var border     : String!
    var color      : String!
    var image      : String!
    var status     : Int!
    var text       : String!
    
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.border = data["border"] as? String
        self.color = data["color"] as? String
        self.image = data["image"] as? String
        self.status = data["status"] as? Int
        self.text = data["text"] as? String
    
    }
}
class dealmodel{
    var image : String!
    var status : Int!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.status = data["status"] as? Int
    }
}
class MHproductdetailzModel{
    var deliveryMode : String!
    var higilight_text : String!
    var expected_delivery : String!
    var favorite : Int!
    var deal : dealmodel!
    var mashoAssured : String!
    var offerPercentage : Int!
    var offerbackground : String!
    var offercolor : String!
    var paymentMode : String!
    var price : String!
    var productName : String!
    var productTitle : String!
    var product_description = [String]()
    var rating : String!
    var ratingCount : Int!
    var returnPolicy : String!
    var reviews : Int!
    var selfies : Int!
    var shipping : String!
    var specialPrice : String!
    var type : String!
    var Disclaimer =  [String]()
    var webshare : String!
    var avalablestock : Int!
    var imagebottomtext : MHProductNewChangeModel!
    var imagelefttext : MHProductNewChangeModel!
    var premiumtags : premiumModel!
    init(fromData data: [String:Any]) {
        self.avalablestock = data["avalablestock"] as? Int
        if let deal = data["deal"] as? [String:Any]{
            self.deal = dealmodel(fromData: deal)
        }
        self.deliveryMode = data["deliveryMode"] as? String
        self.higilight_text = data["higilight_text"] as? String
        self.expected_delivery = data["expected_delivery"] as? String
        self.offerbackground = data["offerbackground"] as? String
        self.offercolor = data["offercolor"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.productName = data["productName"] as? String
        self.productTitle = data["productTitle"] as? String
        if let descAry = data["product_description"] as? [String]{
            for desc in descAry{
            self.product_description.append(desc)
            }
        }
        if let premiumdata = data["premiumtags"] as? [String:Any]{
            self.premiumtags = premiumModel(fromData: premiumdata)
        }
        if let descAry = data["Disclaimer"] as? [String]{
            for desc in descAry{
            self.Disclaimer.append(desc)
            }
        }
//        self.product_description = data["product_description"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
        self.price = data["price"] as? String
        self.rating = data["rating"] as? String
        self.ratingCount = data["ratingCount"] as? Int
        self.reviews = data["reviews"] as? Int
        self.selfies = data["selfies"] as? Int
        self.specialPrice = data["specialPrice"] as? String
        self.type = data["type"] as? String
        self.offerPercentage = data["offerPercentage"] as? Int
        self.favorite = data["favorite"] as? Int
       // self.Disclaimer = data["Disclaimer"] as? String
        self.webshare = data["webshare"] as? String
        if let imagebottomtext = data["imagebottomtext"] as? [String:Any]{
            self.imagebottomtext = MHProductNewChangeModel(fromData: imagebottomtext)
        }
        if let imagelefttext = data["imagelefttext"] as? [String:Any]{
            self.imagelefttext = MHProductNewChangeModel(fromData: imagelefttext)
        }
    }
    
    public init(){}
}
class MHrecentlyViewedModel{
    var body = [MHrecentlyViewedbodyModel]()
    var button : String!
    var heading : String!
    var api_id : Int!
    var apiname : String!
    init(fromData data: [String:Any]) {
        self.button = data["button"] as? String
        self.heading = data["heading"] as? String
        if let id = data["api_id"] as? Int{
            print("id:",id)
            self.api_id = id
        }        
        self.apiname = data["apiname"] as? String
        if let bodyList = data["body"] as? [[String:Any]]{
            body = [MHrecentlyViewedbodyModel]()
            for item in bodyList{
                self.body.append(MHrecentlyViewedbodyModel(fromData: item))
            }
        }
    }
}
class MHrecentlyViewedbodyModel{
    var assured : Int!
    var buttonvalue : String!
    var imageUrl : String!
    var offerPercentage : String!
    var offerbackground : String!
    var offercolor : String!
    var price : String!
    var pricecolor : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var sp_pricecolor : String!
    var specialPrice : String!
    var favorite : Int!
    var deliveryMode : String!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var type : String!
   var mashoAssured : String!
    init(fromData data: [String:Any]) {
        self.assured = data["assured"] as? Int
        self.buttonvalue = data["buttonvalue"] as? String
        self.imageUrl = data["imageUrl"] as? String
        self.offerPercentage = data["offerPercentage"] as? String
        self.offerbackground = data["offerbackground"] as? String
        self.offercolor = data["offercolor"] as? String
        self.price = data["price"] as? String
        self.pricecolor = data["pricecolor"] as? String
        self.productId = data["productId"] as? String
        self.productName = data["productName"] as? String
        self.productTitle = data["productTitle"] as? String
        self.sp_pricecolor = data["sp_pricecolor"] as? String
        self.specialPrice = data["specialPrice"] as? String
        self.favorite = data["favorite"] as? Int
        self.deliveryMode = data["deliveryMode"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type = data["type"] as? String
        self.mashoAssured = data["mashoAssured"] as? String
    }
}
class MHsimilarProductsModel{
    var body = [MHsimilarProductsbodyModel]()
    var button : String!
    var heading : String!
    var apiname : String!
    var api_id : String!
    var apititle : String!
    init(fromData data: [String:Any]) {
        self.button = data["button"] as? String
        self.heading = data["heading"] as? String
        self.apiname = data["apiname"] as? String
        self.api_id = data["api_id"] as? String
        self.apititle = data["apititle"] as? String
        if let bodyList = data["body"] as? [[String:Any]]{
            body = [MHsimilarProductsbodyModel]()
            for item in bodyList{
                self.body.append(MHsimilarProductsbodyModel(fromData: item))
            }
        }
    }
}
class MHsimilarProductsbodyModel{
    var assured : Int!
    var buttonvalue : String!
    var imageUrl : String!
    var offerPercentage : String!
    var offerbackground : String!
    var offercolor : String!
    var price : String!
    var pricecolor : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var sp_pricecolor : String!
    var specialPrice : String!
    var favorite : Int!
    var mashoAssured : String!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var type : String!
    var deliveryMode : String!
    init(fromData data: [String:Any]) {
        self.assured = data["assured"] as? Int
        self.buttonvalue = data["buttonvalue"] as? String
        self.imageUrl = data["imageUrl"] as? String
        self.offerPercentage = data["offerPercentage"] as? String
        self.offerbackground = data["offerbackground"] as? String
        self.offercolor = data["offercolor"] as? String
        self.price = data["price"] as? String
        self.pricecolor = data["pricecolor"] as? String
        self.productId = data["productId"] as? String
        self.productName = data["productName"] as? String
        self.productTitle = data["productTitle"] as? String
        self.sp_pricecolor = data["sp_pricecolor"] as? String
        self.specialPrice = data["specialPrice"] as? String
        self.favorite = data["favorite"] as? Int
        self.mashoAssured = data["mashoAssured"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.type = data["type"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
    }
}
class MHspecificationModel{
    var property_name_english : String!
    var value_name_eng : String!
    init(fromData data: [String:Any]) {
        self.property_name_english = data["property_name_english"] as? String
        self.value_name_eng = data["value_name_eng"] as? String
    }
}
class MHsuggestionListModel{
    var body = [MHsuggestionListbodyModel]()
    var heading : String!
    init(fromData data: [String:Any]) {
        self.heading = data["heading"] as? String
        if let bodyList = data["body"] as? [[String:Any]]{
            body = [MHsuggestionListbodyModel]()
            for item in bodyList{
                self.body.append(MHsuggestionListbodyModel(fromData: item))
            }
        }
    }
}
class MHsuggestionListbodyModel{
    var ListName : String!
    var api_id : String!
    var apiname : String!
    var catsubcat : String!
    var link : String!
    var name : String!
    var productList = [MHsuggestionListbodyproductListModel]()
    var type : Int!
    init(fromData data: [String:Any]) {
        self.ListName = data["ListName"] as? String
        self.api_id = data["api_id"] as? String
        self.apiname = data["apiname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.link = data["link"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? Int
        if let bodyproductList = data["productList"] as? [[String:Any]]{
            productList = [MHsuggestionListbodyproductListModel]()
            for item in bodyproductList{
                self.productList.append(MHsuggestionListbodyproductListModel(fromData: item))
            }
        }
    }
}
class MHsuggestionListbodyproductListModel{
    var ItemId : Int!
    var ItemUrl : String!
    var Productimage : String!
    var buttonval : String!
    var apiname : String!
    var api_id : Int!
    var apiid : String!
    init(fromData data: [String:Any]) {
        self.ItemId = data["ItemId"] as? Int
        self.ItemUrl = data["ItemUrl"] as? String
        self.Productimage = data["Productimage"] as? String
        self.buttonval = data["buttonval"] as? String
        self.apiname = data["apiname"] as? String
        self.api_id = data["api_id"] as? Int
        self.apiid = data["api_id"] as? String
    }
}
class MHSizeChartModel{
    var image : String!
    var review : MHsizeChartReviewModel!
    var sizechart : Int!
    var text : String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.sizechart = data["sizechart"] as? Int
        self.text = data["text"] as? String
        if let reviewList = data["review"] as? [String:Any]{
            self.review = MHsizeChartReviewModel(fromData: reviewList)
        }
    }
}
class MHsizeChartReviewModel{
    var mainheading : String!
    var status : Int!
    var platstoreratingtxt : String!
    var playstorerating : String!
    var reviews = [MHsizeChartReviewArrayModel]()
    var button: MHReviewViewallModel!
    var addreview: MHaddReviewModel!
    init(fromData data: [String:Any]) {
        self.mainheading = data["mainheading"] as? String
        self.status = data["status"] as? Int
        self.platstoreratingtxt = data["platstoreratingtxt"] as? String
        self.playstorerating = data["playstorerating"] as? String
        if let reviewList = data["reviews"] as? [[String:Any]]{
            self.reviews = [MHsizeChartReviewArrayModel]()
            for item in reviewList{
                self.reviews.append(MHsizeChartReviewArrayModel(fromData: item))
            }
        }
        if let buttonList = data["button"] as? [String:Any]{
            self.button = MHReviewViewallModel(fromData: buttonList)
        }
        if let addreviewList = data["addreview"] as? [String:Any]{
            self.addreview = MHaddReviewModel(fromData: addreviewList)
        }
    }
}
class MHaddReviewModel{
    var text : String!
    var color : String!
    var link : String!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.color = data["color"] as? String
        self.link = data["link"] as? String
    }
}
class MHsizeChartReviewArrayModel{
    var image : String!
    var name : String!
    var rating : Int!
    var status : Int!
    var review : String!
    init(fromData data: [String:Any]) {
        self.image = data["image"] as? String
        self.name = data["name"] as? String
        self.rating = data["rating"] as? Int
        self.status = data["status"] as? Int
        self.review = data["review"] as? String
    }
}
class MHReviewViewallModel{
    var text : String!
    var background : String!
    var colour : String!
    var link : String!
    init(fromData data: [String:Any]) {
        self.text = data["text"] as? String
        self.background = data["background"] as? String
        self.colour = data["colour"] as? String
        self.link = data["link"] as? String
    }
}
class MHProductDetailsModel2{
    var bannerbottom = [BannerBottomModel]()
    var recentlyViewed : MHrecentlyViewedModel!
    var review : MHsizeChartReviewModel!
    var similarProducts : MHsimilarProductsModel!
    var suggestionList : MHsuggestionListModel!
    
    init(fromData data: [String:Any]) {
        if let banners = data["bannerbottom"] as? [[String:Any]]{
            for banner in banners {
                self.bannerbottom.append(BannerBottomModel(fromData: banner))
            }
        }
        if let recent = data["recentlyViewed"] as? [String:Any]{
            self.recentlyViewed = MHrecentlyViewedModel(fromData: recent)
        }
        if let reviews = data["review"] as? [String:Any]{
            self.review = MHsizeChartReviewModel(fromData: reviews)
        }
        if let similarProducts = data["similarProducts"] as? [String:Any]{
            self.similarProducts = MHsimilarProductsModel(fromData: similarProducts)
        }
        if let suggestionList = data["suggestionList"] as? [String:Any]{
            self.suggestionList = MHsuggestionListModel(fromData: suggestionList)
        }
        
    }
}
class splitAPI2ratingModel{
    var addreview : String!
    var button : String!
    var mainheading : String!
    var platstoreratingtxt : String!
    var playstorerating : String!
    var reviews : String!
    init(fromData data: [String:Any]) {
        
        
    }
}
class splitAPI2ratingModel2{
    var image : String!
    var name : String!
    var rating : String!
    var review : String!
    init(fromData data: [String:Any]) {
    }
}

class BannerBottomModel{
    var api_id : Int!
    var apiname : String!
    var image : String!
    var link : String!
    var type : Int!
    var catsubcat : String!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? Int
        self.apiname = data["apiname"] as? String
        self.image = data["image"] as? String
        self.link = data["link"] as? String
        self.type = data["type"] as? Int
        self.catsubcat = data["catsubcat"] as? String ?? "sc"
        
        
        
        
    }
}
 
