//
//  MHRegistrationModels.swift
//  masho
//
//  Created by Appzoc on 03/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class LoginModel{
    var otp : Int!
    var message : String!
    init(fromData data: Json) {
        self.otp = data["otp"] as? Int
        self.message = data["message"] as? String
    }
}
class CountryCodeModel{
    var code : String!
    var name : String!
    var flag : String!
    var value: String!
    var selected : Int!
    init(fromData data: Json) {
        self.code = data["code"] as? String
        self.name = data["name"] as? String
        self.value = data["value"] as? String
        self.flag = data["flag"] as? String
        self.selected = data["selected"] as? Int
    }
}
class VerificationModel{
    var device_tble_id : String!
    var mobile : String!
    var otp : String!
    var profileimg : String!
    var userId : String!
    var userName : String!
    init(fromData data: Json) {
        self.device_tble_id = data["device_tble_id"] as? String
        self.mobile = data["mobile"] as? String
        self.otp = data["otp"] as? String
        self.profileimg = data["profileimg"] as? String
        self.userId = data["userId"] as? String
        self.userName = data["userName "] as? String
    }
}
