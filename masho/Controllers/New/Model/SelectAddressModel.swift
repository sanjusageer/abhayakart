//
//  SelectAddressModel.swift
//  masho
//
//  Created by Appzoc on 23/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class viewAddressModel{
    var address : viewAddress_addressModel!
    var btndata :  viewAddress_btndataModel!
    var mainheading : String!
    init(fromData data: [String:Any]) {
        self.mainheading = data["mainheading"] as? String
        if let addres = data["address"] as? [String:Any]{
            self.address = viewAddress_addressModel(fromData: addres)
        }
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = viewAddress_btndataModel(fromData: btndatas)
        }
    }
}
class viewAddress_addressModel{
    var address = [viewAddress_addressDetailsModel]()
    var options = [viewAddress_optionsModel]()
    init(fromData data: [String:Any]) {
        if let addressList = data["address"] as? [[String:Any]]{
            address = [viewAddress_addressDetailsModel]()
            for item in addressList{
                self.address.append(viewAddress_addressDetailsModel(fromData: item))
            }
        }
        if let optionsList = data["options"] as? [[String:Any]]{
            options = [viewAddress_optionsModel]()
            for item in optionsList{
                self.options.append(viewAddress_optionsModel(fromData: item))
            }
        }
    }
}
class viewAddress_addressDetailsModel{
    var address : String!
    var address_id : String!
    var addresstype : String!
    var city : String!
    var country : String!
    var customer_name : String!
    var defultaddress : String!
    var email : String!
    var landmark : String!
    var mobile : String!
    var mobile_code : String!
    var pincode : String!
    var user_id : String!
    var state :String!
    init(fromData data: [String:Any]) {
        self.address = data["address"] as? String
        self.address_id = data["address_id"] as? String
        self.addresstype = data["addresstype"] as? String
        self.defultaddress = data["defultaddress"] as? String
        self.city = data["city"] as? String
        self.country = data["country"] as? String
        self.customer_name = data["customer_name"] as? String
        self.email = data["email"] as? String
        self.landmark = data["landmark"] as? String
        self.mobile = data["mobile"] as? String
        self.mobile_code = data["mobile_code"] as? String
        self.pincode = data["pincode"] as? String
        self.user_id = data["user_id"] as? String
        self.state = data["state"] as? String
    }
}
class viewAddress_optionsModel{
    var apiname : String!
    var btndata : viewAddress_options_btndataModel!
    var fieldtype : String!
    var name : String!
    var value : String!
    init(fromData data: [String:Any]) {
        self.apiname = data["apiname"] as? String
        self.fieldtype = data["fieldtype"] as? String
        self.name = data["name"] as? String
        self.value = data["value"] as? String
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = viewAddress_options_btndataModel(fromData: btndatas)
        }
    }
}
class viewAddress_options_btndataModel{
    var background : String!
    var color : String!
    var icon : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.icon = data["icon"] as? String
    }
    
}
class viewAddress_btndataModel{
    var apiname : String!
    var background : String!
    var color : String!
    var value : String!
    var titledata = [String]()
    init(fromData data: [String:Any]) {
        self.apiname = data["apiname"] as? String
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.value = data["value"] as? String
        if let data = data["titledata"] as? [String]{
            for i in data{
                self.titledata.append(i)
            }
            
        }
    }
}
