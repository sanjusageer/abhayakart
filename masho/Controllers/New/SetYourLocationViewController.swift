//
//  SetYourLocationViewController.swift
//  masho
//
//  Created by WC46 on 24/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class SetYourLocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
@IBOutlet var setyourlocation:UITableView!
    
 var setyourlocationarray = [" Location "," Area "," House / Flat No. "]
    var landmarkarray = [" Name "," Phone Number "," Pincode "]
    var textfiledarray1 = ["Akshya Nagar 1st Block 1st Cross,Rammurthy nagar,Bangalore-560016"," Kochi"," 25"]
     var textfiledarray2 = ["John Do"," 9876543210"," 003210"]
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
          setyourlocation.dataSource = self
        setyourlocation.delegate = self
        //homecollectionView.dataSource = self
       // homecollectionView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ConfirmLocationBTNtapped(_ sender: Any) {
         let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
       self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
    }
    @IBAction func EditBTNtapped(_ sender: Any) {
        let vc = StoryBoard.main.instantiateViewController(withIdentifier: "MHMapVC") as! MHMapVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( section == 3)
        {
            return 3
        }
       else  if ( section == 5)
        {
           return 3
       }
        else
           {
          return  1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ( indexPath.section == 0)
        {
            let cell = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell1",for: indexPath)as!SetYourLocationCell1
    
            
            return cell
        }
        else  if ( indexPath.section == 1)
        {
            let cell2 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell2",for: indexPath)as!SetYourLocationCell2
            
            
            
            return cell2
        }
         else  if ( indexPath.section == 2)
        {
        let cell3 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell3",for: indexPath)as!SetYourLocationCell3
        
        cell3.setyourlocation.text = "Set Your Location"
         
        
        return cell3
        }
        else  if ( indexPath.section == 3)
        {
            let cell4 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell4",for: indexPath)as!SetYourLocationCell4
            cell4.locationsubtitle.text = setyourlocationarray[indexPath.row]
//            cell4.textfiled.text = textfiledarray1[indexPath.row]
            if indexPath.row == 0{
                cell4.ContentTV.isHidden = false
                cell4.textfiled.isHidden = true
                cell4.ContentTV.text = textfiledarray1[indexPath.row]
                cell4.TVHeightConstraint.constant = 65
            }else{
                cell4.ContentTV.isHidden = true
                cell4.textfiled.isHidden = false
                cell4.textfiled.text = textfiledarray1[indexPath.row]
                cell4.TVHeightConstraint.constant = 20
            }
            return cell4
        }
       else  if ( indexPath.section == 4)
       {
        let cell5 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell3",for: indexPath)as!SetYourLocationCell3

     cell5.setyourlocation.text = "Land Mark"

        return cell5
        }
        else  if ( indexPath.section == 5)
        {
            let cell5 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell4",for: indexPath)as!SetYourLocationCell4
            cell5.locationsubtitle.text = landmarkarray[indexPath.row]
           
             cell5.textfiled.text = textfiledarray2[indexPath.row]
            if (textfiledarray2[indexPath.row] == " 9876543210")
            {
                print("123454")
                cell5.textfiled.keyboardType = .numberPad
            }
          else  if (textfiledarray2[indexPath.row] == " 003210")
            {
                print("123454")
                cell5.textfiled.keyboardType = .numberPad
            }
            
            return cell5
        }
        else  if ( indexPath.section == 6)
        {
            let cell6 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell5",for: indexPath)as!SetYourLocationCell5
            
            return cell6
        }
        else  if ( indexPath.section == 7)
        {
            let cell7 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell6",for: indexPath)as!SetYourLocationCell6
//            if index == indexPath.row
//            {
//
//                cell7.savecheckimage.image = UIImage(named:"Checked")
//                cell7.defautcheckimage.image = UIImage(named:"Checked")
//            }else{
            
//                cell7.savecheckimage.layer.borderColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
//                cell7.defautcheckimage.layer.borderColor = #colorLiteral(red: 0.831372549, green: 0.6862745098, blue: 0.2156862745, alpha: 1)
           // }
           
            return cell7
        }
        else
        {
            let cell8 = self.setyourlocation.dequeueReusableCell(withIdentifier:"SetYourLocationCell7",for: indexPath)as!SetYourLocationCell7
            return cell8
        }

    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 9
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        index = indexPath.row
        tableView.reloadData()
        print(indexPath.row)
    }
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return collectionlabelarray.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
//        cel.collectionlabel.text = collectionlabelarray[indexPath.row]
//        return cel
//    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


