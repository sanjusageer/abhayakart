//
//  CategoriesCells.swift
//  masho
//
//  Created by Castler on 27/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import FSPagerView
class PremiumCVC:UICollectionViewCell{
    @IBOutlet weak var PremiumBGvew: UIView!
    @IBOutlet weak var PremiumTxt: UILabel!
    @IBOutlet weak var PremiumImg: UIImageView!
//    @IBOutlet weak var PremiumVewHeightConstraint:NSLayoutConstraint!
}
class CategoriesCVC:UICollectionViewCell{
    @IBOutlet weak var CategoriesLb: UILabel!
    @IBOutlet weak var CategoriesIcon: UIImageView!
    @IBOutlet weak var BackgroundColorView: UIView!
}
class CategoriesTVC:UITableViewCell{
    @IBOutlet weak var CategoriesLb: UILabel!
    @IBOutlet weak var TriangleIcon: UIImageView!
    @IBOutlet weak var ExpansionBTN:UIButton!
    
}
class SubCategoriesTVC:UITableViewCell{
    @IBOutlet weak var SubCategoriesLb: UILabel!
}
class NewCategoriesCVC:UICollectionViewCell{
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var TitleLb: UILabel!
}
class NewCategoriesProductCVC:UICollectionViewCell{
    var delegate: FavoriteProductDelegate!
    var index : Int!
    @IBOutlet weak var Imported_Lb:UILabel!
    @IBOutlet weak var ImportedView: BaseView!
    @IBOutlet weak var Freesize_Lb:UILabel!
    @IBOutlet weak var FreesizeView: BaseView!
    @IBOutlet weak var ImportedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var Productimage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var StrikePrice: UILabel!
    @IBOutlet weak var OfferPrice: UILabel!
    @IBOutlet weak var PercentageLb: UILabel!
    @IBOutlet weak var PercentageView:UIView!
    @IBOutlet weak var DealView:UIView!
    @IBOutlet weak var DealLb:UILabel!
    @IBOutlet weak var FavView: BaseView!
    @IBOutlet weak var FavImg: UIImageView!
    @IBOutlet weak var GridAspectConstraint: NSLayoutConstraint!
    
    @IBAction func FavBTNtapped(_ sender: Any) {
        delegate.didSelectFavorite(indx: index)
    }
}

//MARK:-------------------Filter Cells-------------------------

class FilterMainTVC:UITableViewCell{
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var CountView:UIView!
    @IBOutlet weak var CountLb:UILabel!
    
}
class FilterSubTVC:UITableViewCell{
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var SelectionBTN: UIButton!
    @IBOutlet weak var CheckImg:UIImageView!
}
class FilterDealsOfTheDayCVC:UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FavoriteProductDelegate{
// MARK: - Deal of the day view cell
    //------------------------------------
    var navCtlr :UINavigationController!
    var products = [MHcategoryProduct_ProductModel2]()
    @IBOutlet weak var ProductsCV:UICollectionView!
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var ViewAllLBL: UILabel!
    @IBOutlet weak var ViewAllBorder : BaseView!
    @IBOutlet weak var viwBorder : BaseView!
    @IBOutlet weak var ViewallBTN : UIButton!
    var delegate : FavoriteProductDelegate!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recentProductsInDashboardCVC", for: indexPath) as! recentProductsInDashboardCVC
        cell.delegate = self
        cell.index = indexPath.row
        cell.ProductImage.kf.setImage(with: URL(string: products[indexPath.row].imageUrl))
        cell.ProductName.text = products[indexPath.row].productName
        cell.PriceLb.text = products[indexPath.row].price
        cell.PriceLb.textColor = UIColor.black
        cell.StrikePrice.attributedText = products[indexPath.row].strikeprice.updateStrikeThroughFont(UIColor.red)
        cell.StrikePrice.textColor = UIColor.red
        cell.PrecentageLb.text = "\(products[indexPath.row].offerpercent!)% Off"
        cell.viwPercentage.isHidden = products[indexPath.row].offerpercent == 0
        if products[indexPath.row].favorite == 1{
            cell.FavImage.image = UIImage(named: "favSelected")
        }else{
            cell.FavImage.image = UIImage(named: "favUnselected")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
        //        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .newCategoryDetailsDeals
        MHProductDetailsVC.BasicData9 = self.products[indexPath.row]
//        MHProductDetailsVC.productID = self.products[indexPath.row].productId
        productID = self.products[indexPath.row].productId
        frompush = false
        self.navCtlr?.pushViewController(MHProductDetailsVC, animated: true)
    }
    func didSelectFavorite(indx: Int) {
        
        //FavAPI
//        print("Product Name : \(products[indx].productname) \n \(products[indx].image)")
        let productID = products[indx].productId ?? ""
        let shortList = products[indx].favorite
        let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? "" //userID
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        var type:String = ""
        if shortList == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = ["action":"Shortlist",
                          "productid":"\(productID)",
                          "userid":"\(userID)",
                          "guestId": guest_id,
                          "type":"\(type)",
                          "deviceid": deviceToken,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if self.products[indx].favorite == 0{
                    self.products[indx].favorite = 1
                }else{
                    self.products[indx].favorite = 0
                }
                self.ProductsCV.reloadData()
//                DispatchQueue.main.async {
//                    NotificationCenter.default.post(name: Notification.dashboardContentSetup, object: nil)
//                }
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
            case .failure :
                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
        
    }
}
class FilterBannerCVC:UICollectionViewCell,FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    var banners = [bannerImagesModel]()
    var navCtler : UINavigationController!
    @IBOutlet weak var BannerView: FSPagerView!
    @IBOutlet weak var PageCtl: FSPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        BannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "SliderCell")
        BannerView.itemSize = CGSize(width: BannerView.frame.width, height: BannerView.frame.height)
        BannerView.transformer = FSPagerViewTransformer(type:.zoomOut)
        
        PageCtl.numberOfPages = self.banners.count//dot
        PageCtl.setImage(UIImage(named: "dot"), for: UIControl.State.selected)
        PageCtl.setImage(UIImage(named: "dot1"), for: UIControl.State.normal)
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cel = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderCell", at: index)
//        cel.imageView?.image = UIImage(named: banners[index].image!)
        let urlString = banners[index].image!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        cel.imageView?.contentMode = .scaleAspectFill
        cel.imageView?.kf.setImage(with: URL(string: urlString))
        return cel
    }
    
//    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
//        return false
//    }
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        if pagerView == self.BannerView{
            self.PageCtl.currentPage = pagerView.currentIndex
        }
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if banners[index].type == 1{
            if banners[index].apiname == "homepagedetails"{
                let navigationStack = navCtler?.viewControllers ?? []
                for vc in navigationStack{
                    if let dashboardVC = vc as? MHdashBoardVC{
                        DispatchQueue.main.async {
                            self.navCtler?.popToViewController(dashboardVC, animated: true)
                        }
                    }
                }
            }else if banners[index].apiname == "getProductsByCatId"{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                vc.categoryId = "\(banners[index].api_id!)"
                vc.SubCategoryId = banners[index].catsubcat
                navCtler?.pushViewController(vc, animated: true)
            }else if banners[index].apiname == "getCartItems" {
                let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                //                CartVC.categoryArray = self.categoryListArray
                frompush = false
                navCtler?.pushViewController(CartVC, animated: true)
            }else if banners[index].apiname == "getProductDetails" {
                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                //        MHProductDetailsVC.categoryArray = categoryArray
                MHProductDetailsVC.isFrom = .other
//                MHProductDetailsVC.productID = String(banners[index].api_id)
                productID = String(banners[index].api_id)
                 frompush = false
                navCtler!.pushViewController(MHProductDetailsVC, animated: true)
            }else{
                //
            }
        }else if banners[index].type == 2{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            WebViewVC.WebURL = banners[index].link
            navCtler!.pushViewController(WebViewVC, animated: true)
        }else{
            if let newURL = URL(string: banners[index].link){
                UIApplication.shared.open(newURL)
            }
        }
    }
    
}
class SortTVC:UITableViewCell{
    @IBOutlet weak var TitleLb: UILabel!
    @IBOutlet weak var img:UIImageView!
}
