//
//  MHCategoriesVC.swift
//  masho
//
//  Created by Castler on 27/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
class MHCategoriesVC: UIViewController {
    var expansionSection : Int?
    var PrevexpansionSection : Int?
    var Device_token : String!
    var UserID : String!
    var TableID : String!
    var userName : String!
    var guest_id : Int! = nil
    var isFromSidemenu = false
    var CategoryData : CategoriesModel!
    var colorArray = [UIColor.red,
                      UIColor.green,
                      UIColor.blue,
                      UIColor.orange,
                      UIColor.brown,
                      UIColor.cyan,
                      UIColor.green,
                      UIColor.magenta,
                      UIColor.purple,
                      UIColor.darkGray,
                      UIColor.black,
                      UIColor.red,
                      UIColor.green,
                      UIColor.blue,
                      UIColor.orange,
                      UIColor.brown,
                      UIColor.cyan,
                      UIColor.green,
                      UIColor.magenta,
                      UIColor.purple,
                      UIColor.darkGray,
                      UIColor.black]
    @IBOutlet weak var EmptyView:UIView!
    @IBOutlet weak var CategoryCV:UICollectionView!
    @IBOutlet weak var CategoryCVHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var CategoryTV:UITableView!
    @IBOutlet weak var CategoryTVHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    @IBOutlet weak var MainScroll:UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        MainScroll.delegate = self
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        getCategories()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == nil || cartitemsCount == 0{
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
    }
    
    @IBAction func homebtnTApped(_ sender: Any) {
        
        KVSpinnerView.dismiss()
        let navigationStack = navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: false)
                }
            }
        }
    }
    
    @IBAction func CartBTNtapped(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//        WebViewVC.categoryArray = self.categoryArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    
    @IBAction func BackBTNtapped(_ sender: Any) {
        if isFromSidemenu{
            self.navigationController?.popViewController(animated: true)
        }else{
                let navigationStack = navigationController?.viewControllers ?? []
                for vc in navigationStack{
                    if let dashboardVC = vc as? MHdashBoardVC{
                        DispatchQueue.main.async {
                            self.navigationController?.popToViewController(dashboardVC, animated: true)
                        }
                    }
                }
        }
    }
    @IBAction func searchBTNTapped(_ sender: Any) {
        let search = StoryBoard.login.instantiateViewController(withIdentifier: "searchVC") as! searchVC
        self.navigationController?.pushViewController(search, animated: true)
        
    }
    @IBAction func ExpansionBTNtapped(_ sender: UIButton) {
        print(sender.tag)
        if expansionSection == sender.tag{
            expansionSection =  nil
            let sectionHeight = 55 * self.CategoryData.subcategory.maincategory.count
            CategoryTVHeightConstraint.constant = CGFloat(sectionHeight + 5)
        }else{
            expansionSection = sender.tag
            let sectionHeight = 55 * self.CategoryData.subcategory.maincategory.count
            let SelectedContentHeight = 55 *  self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys.count
            CategoryTVHeightConstraint.constant = CGFloat(sectionHeight + SelectedContentHeight + 5)
        }
        CategoryTV.reloadData()
        if sender.tag == 0{
            MainScroll.contentOffset = CGPoint(x: 0.0, y: self.CategoryCV.frame.height + 50)
        }else{
            MainScroll.contentOffset = CGPoint(x: 0.0, y: self.CategoryCV.frame.height + 50 + CGFloat((sender.tag * 55)));
        }
    }
    deinit {
           DebugLogger.debug("deinitilizing :\(#file) [😀]")
       }
}
extension MHCategoriesVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.CategoryData != nil{
            return self.CategoryData.maincategory.datas.count
        }else{
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCVC", for: indexPath) as! CategoriesCVC
        if self.CategoryData != nil{
            cell.CategoriesLb.text = self.CategoryData.maincategory.datas[indexPath.row].name
            cell.CategoriesIcon.kf.setImage(with: URL(string: self.CategoryData.maincategory.datas[indexPath.row].image!))
            cell.BackgroundColorView.backgroundColor = colorArray[indexPath.row].withAlphaComponent(0.1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3
        let height = width * (100/105)
        if self.CategoryData != nil{
            if self.CategoryData.maincategory.datas.count % 3 == 0{
                CategoryCVHeightConstraint.constant = height * CGFloat(self.CategoryData.maincategory.datas.count / 3)
            }else{
                CategoryCVHeightConstraint.constant = height * CGFloat((self.CategoryData.maincategory.datas.count / 3) + 1)
            }
        }
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
        vc.categoryId = "\(self.CategoryData.maincategory.datas[indexPath.row].api_id!)"
        vc.SubCategoryId = self.CategoryData.maincategory.datas[indexPath.row].catsubcat
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == MainScroll{
            print(scrollView.contentOffset.y)
        }
    }
}
extension MHCategoriesVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.CategoryData != nil{
            return self.CategoryData.subcategory.maincategory[section].subcategorys.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesTVC") as! SubCategoriesTVC
        if self.CategoryData != nil{
            cell.SubCategoriesLb.text = self.CategoryData.subcategory.maincategory[indexPath.section].subcategorys[indexPath.row].name
        }
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.CategoryData != nil{
            return self.CategoryData.subcategory.maincategory.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTVC") as! CategoriesTVC
        if self.CategoryData != nil{
            cell.ExpansionBTN.tag = section
            cell.CategoriesLb.text = self.CategoryData.subcategory.maincategory[section].name
            if section == expansionSection{
                cell.TriangleIcon.isHidden = false
            }else{
                cell.TriangleIcon.isHidden = true
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if expansionSection == indexPath.section{
            return 55
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].type == 1{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
            vc.categoryId = self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].api_id
            vc.SubCategoryId =             self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].catsubcat
            self.navigationController?.pushViewController(vc, animated: true)
        }else if self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].type == 2{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            WebViewVC.WebURL = self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].link
            self.navigationController!.pushViewController(WebViewVC, animated: true)
        }else{
            if let newURL = URL(string: self.CategoryData.subcategory.maincategory[expansionSection!].subcategorys[indexPath.row].link){
                UIApplication.shared.open(newURL)
            }
        }
        
    }
}
extension MHCategoriesVC{
    func getCategories(){
        //
        let parameters = ["action":"categorys",
                          "userid": self.UserID!,
                          "tableid":self.TableID!,
                          "guestId": guest_id!] as [String : Any]
        EmptyView.isHidden = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
                KVSpinnerView.dismiss()
            case .success :
                print("success")
                print(response)
                if let data = response["data"] as? [String : Any]{
                    self.CategoryData = CategoriesModel(fromData: data)
                }
                let sectionHeight = 55 * self.CategoryData.subcategory.maincategory.count
                let SelectedContentHeight = 0
                self.CategoryTVHeightConstraint.constant = CGFloat(sectionHeight + SelectedContentHeight + 5)
                self.EmptyView.isHidden = true
                KVSpinnerView.dismiss()
                self.colorArray.shuffle()
                self.CategoryCV.reloadData()
                self.CategoryTV.reloadData()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
                KVSpinnerView.dismiss()
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                KVSpinnerView.dismiss()
            }
        }
    }
}
