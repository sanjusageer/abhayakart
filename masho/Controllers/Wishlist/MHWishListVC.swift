//
//  MHWishListVC.swift
//  masho
//
//  Created by Appzoc Technologies on 18/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Firebase
import DropDown
class MHWishListVC: UIViewController, RedloadList {
    
    var dropDown = DropDown()
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var tableRef: UITableView!
    
    @IBOutlet weak var listEmptyIndicatorLBL: UILabel!
    var categoryArray = [categoryListmodel]()
    var dataSource:[MHWishListDataModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWebcall()
        setDropdown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        logEvent()
    }
    func logEvent(){
        Analytics.logEvent("Wishlist_Opened", parameters: [:])
    }
    
    func reloadList() {
       setUpWebcall()
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        
        if frompush {
           
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
        
        else {
           self.navigationController?.popViewController(animated: true)
        }
       
    }
    
    func setUpWebcall(){
        let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        
        let parameters = ["action":"favouriteList",
                          "deviceid": deviceToken,
                          "devicetype": 2,
                          "userid":"\(userID)",
                          "guest_Id":guest_id,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                self.dataSource.removeAll()
                if let data = response["data"] as? [[String:Any]]{
                    for item in data{
                        self.dataSource.append(MHWishListDataModel(fromData: item))
                    }
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                self.tableRef.reloadData()
//                do{
//                    let data = try JSONSerialization.data(withJSONObject: response["data"] as Any, options: .prettyPrinted)
//                    let parsedData = try JSONDecoder().decode([MHWishListDataModel].self, from: data)
//                    self.dataSource = parsedData
//                    DispatchQueue.main.async {
//                        self.tableRef.reloadData()
//                    }
//                }catch{
//                    print(error)
//                }
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
    }
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                    CartVC.categoryArray = self.categoryArray
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
}

extension MHWishListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0{
            DispatchQueue.main.async {
                self.listEmptyIndicatorLBL.isHidden = false
            }
        }else{
            DispatchQueue.main.async {
                self.listEmptyIndicatorLBL.isHidden = true
            }
        }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MHWishListTVC") as! MHWishListTVC
        let product = dataSource[indexPath.row]
        cell.productNameLBL.text = product.productname
        cell.strikePriceLBL.attributedText = "\(product.strikeprice!)".updateStrikeThroughFont(UIColor.black.withAlphaComponent(0.3))
        cell.priceLBL.text = "\(product.specialprice!)"
        cell.productImageView.kf.setImage(with: URL(string: product.image))
        let productIDNumeric = Int(product.productid) ?? 0
        cell.deleteBTNRef.tag = productIDNumeric
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .wishlist
        MHProductDetailsVC.BasicData4 = dataSource[indexPath.row]
        productID = dataSource[indexPath.row].productid
        frompush = false
        self.navigationController!.pushViewController(MHProductDetailsVC, animated: true)
    }
}


protocol RedloadList{
    func reloadList()
}
